// ignore_for_file: library_private_types_in_public_api, use_key_in_widget_constructors
import 'package:flutter/material.dart';

import '../../../../settings/language_options.dart';
import '../../../../settings/settings_option.dart';
import '../../../../trans/translations.dart';
import '../../../../utils/app_theme.dart';
import '../../../widgets/custom_text_button.dart';
import '../../../widgets/default_button.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  // FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      body: SingleChildScrollView(
        child: SizedBox(
          height: size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: size.height * .4,
                    width: size.width,
                    child: Image.asset(
                      'assets/images/Aby-seyara-blue.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Text(
                        Translations.of(context)!.text("welcome_in_app"),
                        style: AppTheme.blueHeadline),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    Translations.of(context)!.text("better_time"),
                    style: AppTheme.blueHeadline.copyWith(fontSize: 18),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              // SizedBox(
              //   height: size.height * .1,
              // ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    DefaultButton(
                      text: Translations.of(context)!.text("signup"),
                      press: () {
                        Navigator.of(context).pushNamed("/sign_up");
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    CustomTextButton(
                      text: Translations.of(context)!.text("login"),
                      onPressed: () {
                        Navigator.of(context).pushNamed("/login");
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LanguageItem extends StatelessWidget {
  const LanguageItem(this.options, this.onOptionsChanged);

  final SettingsOptions options;
  final ValueChanged<SettingsOptions> onOptionsChanged;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: <Widget>[
          const SizedBox(width: 4),
          PopupMenuButton<AppLanguageValue>(
            padding: const EdgeInsetsDirectional.only(end: 16.0),
            icon: const Icon(
              Icons.language,
              color: Colors.white,
            ),
            itemBuilder: (BuildContext context) {
              return appLanguageValues.map((appLanguageValue) {
                return PopupMenuItem<AppLanguageValue>(
                  value: appLanguageValue,
                  child: Text(appLanguageValue.language!),
                );
              }).toList();
            },
            onSelected: (AppLanguageValue scaleValue) {
              Translations.load(scaleValue.locale!).then((local) {
                onOptionsChanged(
                  options.copyWith(
                    languageLocale: scaleValue.locale,
                    textDirection: scaleValue.textDirection,
                  ),
                );
              });
            },
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Text(Translations.of(context)!.text('language')),
              Text(
                options.languageLocale!.languageCode == 'en'
                    ? "English"
                    : "اللغة العربية",
                style: AppTheme.whiteText,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
