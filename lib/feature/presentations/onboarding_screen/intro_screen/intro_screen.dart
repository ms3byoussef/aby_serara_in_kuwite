import 'package:flutter/material.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';

import '../../../../trans/translations.dart';
import '../../../../utils/app_theme.dart';
import 'components/intro_content.dart';

class OnboardingView extends StatefulWidget {
  const OnboardingView({Key? key}) : super(key: key);

  @override
  State<OnboardingView> createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingView> {
  int currentPage = 0;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: size.height - 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(31),
              color: AppTheme.backgroundColor,
            ),
            child: Column(
              children: [
                SizedBox(
                  height: size.height * .7,
                  child: PageView(
                    onPageChanged: ((value) => setState(() {
                          currentPage = value;
                        })),
                    children: [
                      IntroContent(
                        introText:
                            Translations.of(context)!.text("first_intro_title"),
                        imgName: 'car-safety-edit.gif',
                        hintText:
                            Translations.of(context)!.text("first_intro_body"),
                      ),
                      IntroContent(
                        introText: Translations.of(context)!
                            .text("second_intro_title"),
                        imgName: 'car_test.jpg',
                        hintText:
                            Translations.of(context)!.text("second_intro_body"),
                      ),
                      IntroContent(
                        introText:
                            Translations.of(context)!.text("third_intro_title"),
                        imgName: 'car_loan.jpg',
                        hintText:
                            Translations.of(context)!.text("third_intro_body"),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: size.height * .05,
                ),
                SizedBox(
                  height: size.height * .05,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: List.generate(
                      3,
                      (index) => buildDot(index),
                    ),
                  ),
                ),
                SizedBox(height: size.height * .05),
              ],
            ),
          ),
        ),
      ),
      bottomSheet: ClipPath(
        clipper: ProsteBezierCurve(
          position: ClipPosition.top,
          list: [
            BezierCurveSection(
              start: Offset(size.width, 0),
              top: Offset(size.width / 2, 30),
              end: const Offset(0, 0),
            ),
          ],
        ),
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed("/welcome");
          },
          child: Container(
            padding: const EdgeInsets.only(top: 10),
            height: size.height * .15,
            width: size.width,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xff3c80f7), Color(0xff1058d1)],
                stops: [0, 1],
                begin: Alignment(-1.00, 0.00),
                end: Alignment(1.00, -0.00),
              ),
            ),
            child: Center(
              child: Text(
                Translations.of(context)!.text("get_start"),
                style: AppTheme.whiteText.copyWith(fontSize: 22),
              ),
            ),
          ),
        ),
      ),
    );
  }

  AnimatedContainer buildDot(int index) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      margin: const EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
          color: currentPage == index ? AppTheme.primaryColor : Colors.teal,
          borderRadius: BorderRadius.circular(3)),
    );
  }
}
