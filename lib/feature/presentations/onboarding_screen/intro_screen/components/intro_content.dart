import 'package:flutter/material.dart';

import '../../../../../utils/app_theme.dart';

class IntroContent extends StatelessWidget {
  final String? imgName;
  final String? introText;
  final String? hintText;
  const IntroContent({
    Key? key,
    this.imgName,
    this.introText,
    this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          "assets/images/$imgName",
          fit: BoxFit.cover,
          height: MediaQuery.of(context).size.height * .5,
        ),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            introText!,
            textAlign: TextAlign.center,
            style: AppTheme.introText.copyWith(fontSize: 20),
            maxLines: 2,
          ),
        ),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            hintText!,
            textAlign: TextAlign.center,
            style: AppTheme.hintText,
            maxLines: 3,
          ),
        ),
      ],
    );
  }
}
