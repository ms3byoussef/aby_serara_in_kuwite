// ignore_for_file: prefer_typing_uninitialized_variables, no_leading_underscores_for_local_identifiers

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../data/models/user_model.dart';
import '../../../../redux/app/app_state.dart';
import '../../auth/auth_view_model.dart';
import '../../users_screens/admin_screens/admin_main_view.dart';
import '../../users_screens/client_screens/client_main_view.dart';
import '../../users_screens/company_screens/company_main_view.dart';
import '../intro_screen/intro_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      builder: (_, viewModel) => _SplashScreen(
        viewModel: viewModel,
      ),
      converter: (store) {
        return AuthViewModel.fromStore(store);
      },
    );
  }
}

class _SplashScreen extends StatefulWidget {
  final AuthViewModel? viewModel;
  const _SplashScreen({this.viewModel});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<_SplashScreen> {
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<Timer> loadData() async {
    return Timer(const Duration(seconds: 3), onDoneLoading);
  }

  onDoneLoading() async {
    var userJson;
    var user;
    SharedPreferences.getInstance().then((prefs) {
      bool? isLogin = prefs.getBool("isLogin");
      if (isLogin == null) {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => const OnboardingView(),
          ),
        );
      } else if (!isLogin) {
        Navigator.of(context)
            .pushNamedAndRemoveUntil("/login", (route) => false);
      } else {
        // print(widget.viewModel!.user!.role);
        const JsonDecoder _decoder = JsonDecoder();
        userJson = _decoder.convert(prefs.getString("users") ?? "");
        setState(() {
          user = UserModel.fromJson(userJson);
          widget.viewModel!.setUser!(user);
        });
        if (user!.role == 'admin') {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (_) => const AdminMainView()),
              (route) => false);
        } else {
          if (user!.role == 'client') {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (_) => const ClientMainView()),
                (route) => false);
          } else {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (_) => const CompanyMainView()),
                (route) => false);
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      //  Color(0xFFF7F7F7),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xff3c80f7), Color(0xff1058d1)],
                    stops: [0, 1],
                    begin: Alignment(-1.00, 0.00),
                    end: Alignment(1.00, -0.00),
                    // angle: 90,
                    // scale: undefined,
                  ),
                  image: DecorationImage(
                      image: AssetImage("assets/images/Aby seyara.png"),
                      fit: BoxFit.contain),
                ),
                // margin: const EdgeInsets.all(20),
                // padding: const EdgeInsets.all(30),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
