// ignore_for_file: library_private_types_in_public_api, avoid_print, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../../utils/validator.dart';
import '../../../../widgets/custom_form_field.dart';
import '../../../../widgets/custom_text_button.dart';
import '../../../../widgets/password_form_field.dart';
import '../../../users_screens/admin_screens/admin_main_view.dart';
import '../../../users_screens/client_screens/client_main_view.dart';
import '../../../users_screens/company_screens/company_main_view.dart';
import '../../auth_view_model.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      builder: (_, viewModel) => LoginFormContent(
        viewModel: viewModel,
      ),
      converter: (store) {
        return AuthViewModel.fromStore(store);
      },
    );
  }
}

class LoginFormContent extends StatefulWidget {
  final AuthViewModel? viewModel;

  const LoginFormContent({
    Key? key,
    this.viewModel,
  }) : super(key: key);
  @override
  _LoginFormContentState createState() => _LoginFormContentState();
}

class _LoginFormContentState extends State<LoginFormContent> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  ProgressDialog? progressDialog;
  bool _rememberMe = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(LoginFormContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel!.getLoginReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            Translations.of(context)!.text("loading_login"),
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel!.getLoginReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context,
          CustomSnackBar.error(
            message: widget.viewModel!.getLoginReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel!.getLoginReport?.status ==
          ActionStatus.complete) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        widget.viewModel!.getLoginReport?.status = null;

        if (widget.viewModel!.user?.role == "admin") {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (_) => const AdminMainView()),
              (route) => false);
        } else if (widget.viewModel!.user!.role == 'client') {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (_) => const ClientMainView()),
              (route) => false);
        } else if (widget.viewModel!.user!.role == 'company') {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (_) => const CompanyMainView()),
              (route) => false);
        }
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  Widget _buildEmailTF() {
    return CustomFormField(
      label: Translations.of(context)!.text("email"),
      controller: email,
      hintText: Translations.of(context)!.text("hint_email"),
      keyboardType: TextInputType.emailAddress,
      validator: (value) => Validator.validateEmail(value!),
      prefixIcon: const Icon(
        Icons.email,
        color: Colors.white,
      ),
    );
  }

  Widget _buildPasswordTF() {
    return PasswordFormField(
      label: Translations.of(context)!.text("password"),
      controller: password,
      hintText: Translations.of(context)!.text("hint_password"),
      validator: (value) => Validator.validatePassword(value!),
      prefixIcon: const Icon(
        Icons.lock,
        color: Colors.white,
      ),
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () => Navigator.pushNamed(context, "/reset_password"),
        child: Text(
          Translations.of(context)!.text("forget_password"),
          style: AppTheme.whiteText,
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckbox() {
    return SizedBox(
      height: 30.0,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
              value: _rememberMe,
              checkColor: Colors.green,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  _rememberMe = value!;
                });
              },
            ),
          ),
          Text(
            Translations.of(context)!.text("remember_me"),
            style: AppTheme.whiteText,
          ),
        ],
      ),
    );
  }

  void login() async {
    if (email.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_email_empty"),
        ),
      );
      return;
    }
    if (password.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_password_empty"),
        ),
      );
      return;
    }

    if (_formKey.currentState!.validate()) {
      await widget.viewModel?.login!(email.text, password.text);
    } else {
      // ignore: avoid_print
      print("Not Valid");
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("password"),
        ),
      );
    }
  }

  Widget _buildLoginBtn() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: CustomTextButton(
        onPressed: login,
        text: Translations.of(context)!.text("login"),
      ),
    );
  }

  Widget _buildSignUpWithText() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: Column(
        children: [
          Text(
            Translations.of(context)!.text("or"),
            style: AppTheme.whiteText.copyWith(fontSize: 18),
          ),
          const SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '${Translations.of(context)!.text("dont_have_account")}  ',
                style: AppTheme.whiteText
                    .copyWith(fontSize: 18, color: AppTheme.backgroundColor),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/sign_up");
                },
                child: Text(
                  Translations.of(context)!.text("signup"),
                  style: AppTheme.whiteText.copyWith(fontSize: 21),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              _buildEmailTF(),
              _buildPasswordTF(),
              _buildForgotPasswordBtn(),
              _buildRememberMeCheckbox(),
              _buildLoginBtn(),
              _buildSignUpWithText(),
            ],
          ),
        ));
  }
}
