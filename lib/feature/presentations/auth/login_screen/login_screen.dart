// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../settings/settings_option.dart';
import '../../../../trans/translations.dart';
import '../../../../utils/app_theme.dart';
import '../../onboarding_screen/welcome_screen/welcome_screen.dart';
import 'components/login_form.dart';

class LoginScreen extends StatelessWidget {
  final SettingsOptions options;
  final ValueChanged<SettingsOptions> onOptionsChanged;
  const LoginScreen(
      {Key? key, required this.options, required this.onOptionsChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            height: double.infinity,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff1159D2),
                  Color(0xFF61A4F1),
                  Color(0xff1159D2),
                  Color(0xFF478DE0),
                ],
                stops: [0.1, 0.4, 0.8, 0.9],
              ),
            ),
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              padding: const EdgeInsets.symmetric(
                vertical: 20.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 40),
                    child: LanguageItem(options, onOptionsChanged),
                  ),
                  Text(Translations.of(context)!.text("login"),
                      textAlign: TextAlign.center,
                      style: AppTheme.whiteText
                          .copyWith(fontSize: 30, fontWeight: FontWeight.bold)),
                  const SizedBox(height: 30.0),
                  const LoginForm(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
