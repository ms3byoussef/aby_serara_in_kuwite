import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../data/models/user_model.dart';
import '../../../redux/action_report.dart';
import '../../../redux/app/app_state.dart';
import '../../../redux/auth/auth_actions.dart';

class AuthViewModel {
  final ActionReport? getLoginReport;
  final ActionReport? getSignUpReport;
  final ActionReport? getAddressReport;
  final ActionReport? getUpdateProfileReport;
  final ActionReport? addCompanyReport;
  final ActionReport? getAllCompaniesReport;
  final ActionReport? searchCompanyResultReport;
  final ActionReport? getRemoveCompanyReport;

  final Function(UserModel)? setUser;
  final UserModel? user;
  final Function()? signOut;
  final Function(LatLng)? getPlace;
  final String? address;
  final Function(String, String)? login;
  final Function(UserModel, String)? signUp;
  final Function(UserModel, {File img})? updateProfile;
  final Function(UserModel, String password, File)? addCompany;
  final Function()? getCompanies;
  final Function(String text)? searchCompanyResult;
  final Function(String companyId)? removeCompany;

  double? latitude;
  double? longitude;
  final Function(double)? syncLatitude;
  final Function(double)? syncLongitude;

  AuthViewModel(
      {this.setUser,
      this.user,
      this.addCompanyReport,
      this.getAllCompaniesReport,
      this.searchCompanyResultReport,
      this.getRemoveCompanyReport,
      this.addCompany,
      this.getCompanies,
      this.searchCompanyResult,
      this.removeCompany,
      this.login,
      this.signUp,
      this.updateProfile,
      this.getLoginReport,
      this.getSignUpReport,
      this.getAddressReport,
      this.getUpdateProfileReport,
      this.syncLatitude,
      this.latitude,
      this.syncLongitude,
      this.longitude,
      this.signOut,
      this.getPlace,
      this.address});

  static AuthViewModel fromStore(Store<AppState>? store) {
    return AuthViewModel(
      user: store!.state.authState?.user,
      login: (email, password) {
        store.dispatch(
          LoginAction(email: email, password: password),
        );
      },
      setUser: (user) {
        store.dispatch(SyncUserAction(user));
      },
      signUp: (user, password) => store.dispatch(
        SignUpAction(
          user: user,
          password: password,
        ),
      ),
      updateProfile: (UserModel userModel, {File? img}) => store.dispatch(
        UpdateProfileAction(user: userModel, img: img),
      ),
      addCompany: (UserModel company, String password, File img) =>
          store.dispatch(
        AddCompanyAction(company: company, password: password, img: img),
      ),
      getCompanies: () => store.dispatch(
        GetCompaniesAction(),
      ),
      removeCompany: (String companyId) => store.dispatch(
        RemoveCompanyAction(companyId: companyId),
      ),
      signOut: () async {
        SharedPreferences preferences;
        preferences = await SharedPreferences.getInstance();
        preferences.setBool('isLogin', false);

        await FirebaseAuth.instance.signOut();
      },
      getPlace: (latLong) {
        store.dispatch(AddressAction(latLong: latLong));
      },
      address: store.state.authState!.address,
      getLoginReport: store.state.authState?.status!["LoginAction"],
      getSignUpReport: store.state.authState?.status!["SignUpAction"],
      getUpdateProfileReport:
          store.state.authState?.status!["UpdateProfileAction"],
      getAddressReport: store.state.authState?.status!["AddressAction"],
      addCompanyReport: store.state.authState?.status!["AddCompanyAction"],
      getAllCompaniesReport:
          store.state.authState?.status!["GetCompaniesAction"],
      getRemoveCompanyReport:
          store.state.authState?.status!["RemoveCompanyAction"],
    );
  }
}
