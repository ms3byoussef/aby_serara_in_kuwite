import 'package:aby_seyara/utils/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../widgets/car_card_widget/car_card.dart';
import '../../client_screens/client_view_model.dart';

class CompanyHomeScreen extends StatelessWidget {
  const CompanyHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _CompanyHomeScreen(
        viewModel: viewModel,
      ),
    );
  }
}

class _CompanyHomeScreen extends StatefulWidget {
  final ClientViewModel viewModel;
  const _CompanyHomeScreen({Key? key, required this.viewModel})
      : super(key: key);

  @override
  State<_CompanyHomeScreen> createState() => _CompanyHomeScreenState();
}

class _CompanyHomeScreenState extends State<_CompanyHomeScreen> {
  @override
  void initState() {
    widget.viewModel.getCars!();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: size.height * 1.09,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(31),
                gradient: const LinearGradient(
                  colors: [Color(0xff3c80f7), Color(0xff1058d1)],
                  stops: [0, 1],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 24),
                    child: Text(
                      "${Translations.of(context)!.text("hi")}  ${widget.viewModel.currentUser!.name}",
                      style:
                          AppTheme.introHeadline.copyWith(color: Colors.white),
                    ),
                  ),
                  Container(
                    height: size.height - 150,
                    decoration: const BoxDecoration(
                      color: Color(0xffeff5ff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        Flexible(
                          child: widget.viewModel.getCarsReport?.status ==
                                  ActionStatus.running
                              ? const Center(child: CircularProgressIndicator())
                              : widget.viewModel.cars!.isEmpty
                                  ? Center(
                                      child: Text(Translations.of(context)!
                                          .text("error_car_empty")))
                                  : ListView.builder(
                                      scrollDirection: Axis.vertical,
                                      itemCount: widget.viewModel.cars!.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return (widget.viewModel.cars![index]
                                                    .companyID ==
                                                widget
                                                    .viewModel.currentUser!.id)
                                            ? CarCard(
                                                car: widget
                                                    .viewModel.cars![index])
                                            : const SizedBox();
                                      },
                                    ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
