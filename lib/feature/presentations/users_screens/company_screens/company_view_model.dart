import 'package:aby_seyara/data/models/terms_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../data/models/car_model.dart';
import '../../../../data/models/user_model.dart';
import '../../../../redux/action_report.dart';
import '../../../../redux/app/app_state.dart';
import '../../../../redux/company/company_actions.dart';

class CompanyViewModel {
  final ActionReport? postCarReport;
  final ActionReport? getCarsReport;
  final ActionReport? editCarReport;
  final ActionReport? postTermsReport;
  final ActionReport? getTermsReport;
  final ActionReport? editTermsReport;
  final ActionReport? removeTermsReport;
  final ActionReport? removeCarReport;

  final Function(UserModel)? setUser;
  final UserModel? user;
  final Function()? signOut;
  final Function(CarModel)? postCar;
  final Function()? getCars;
  final Function(String)? getCar;
  final Function(CarModel car)? editCar;
  final Function(String)? removeCar;
  final Function(TermsModel)? postTerms;
  final Function()? getAllTerms;
  final Function(String)? getTerms;
  final Function(TermsModel termsModel)? editTerms;
  final Function(String)? removeTerms;
  final List<TermsModel>? allTerms;
  final TermsModel? terms;

  final List<CarModel>? cars;
  final CarModel? car;

  CompanyViewModel({
    this.postTermsReport,
    this.getTermsReport,
    this.editTermsReport,
    this.removeTermsReport,
    this.postTerms,
    this.getAllTerms,
    this.getTerms,
    this.editTerms,
    this.removeTerms,
    this.allTerms,
    this.terms,
    this.postCarReport,
    this.getCarsReport,
    this.editCarReport,
    this.removeCarReport,
    this.postCar,
    this.getCars,
    this.getCar,
    this.editCar,
    this.removeCar,
    this.setUser,
    this.user,
    this.cars,
    this.car,
    this.signOut,
  });

  static CompanyViewModel fromStore(Store<AppState>? store) {
    return CompanyViewModel(
      user: store!.state.authState?.user,
      car: store.state.companyState!.car,
      cars: store.state.companyState!.cars.values.toList(),
      terms: store.state.companyState!.terms,
      allTerms: store.state.companyState!.allTerms!.values.toList(),
      postCar: (car) {
        store.dispatch(PostCarAction(
          car: car,
        ));
      },
      getCars: () => store.dispatch(GetCarsAction()),
      getCar: (carId) => store.dispatch(GetCarAction(carId: carId)),
      editCar: (car) {
        store.dispatch(EditCarAction(
          car: car,
        ));
      },
      removeCar: (carId) {
        store.dispatch(RemoveCarAction(carId: carId));
      },
      postTerms: (TermsModel termsModel) {
        store.dispatch(PostTermsAction(
          terms: termsModel,
        ));
      },
      getAllTerms: () => store.dispatch(GetCarsAction()),
      getTerms: (type) => store.dispatch(GetTermsAction(type: type)),
      editTerms: (terms) {
        store.dispatch(EditTermsAction(
          terms: terms,
        ));
      },
      removeTerms: (termsID) {
        store.dispatch(RemoveTermsAction(termsID: termsID));
      },
      signOut: () async {
        SharedPreferences preferences;
        preferences = await SharedPreferences.getInstance();
        preferences.setBool('isLogin', false);

        await FirebaseAuth.instance.signOut();
      },
      postCarReport: store.state.companyState?.status["PostCarAction"],
      getCarsReport: store.state.companyState?.status["GetCarsAction"],
      editCarReport: store.state.companyState?.status["EditCarAction"],
      removeCarReport: store.state.companyState?.status["RemoveCarAction"],
      postTermsReport: store.state.companyState?.status["PostTermsAction"],
      getTermsReport: store.state.companyState?.status["GetAllTermsAction"],
      editTermsReport: store.state.companyState?.status["EditTermsAction"],
      removeTermsReport: store.state.companyState?.status["RemoveTermsAction"],
    );
  }
}
