import 'package:clay_containers/constants.dart';
import 'package:clay_containers/widgets/clay_container.dart';
import 'package:flutter/material.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

import '../../../../trans/translations.dart';
import '../components/profile/profile_screen.dart';
import '../components/requests_screen/requests_screen.dart';
import 'add_car_screen/add_car_screen.dart';
import 'company_home_screen/company_home_screen.dart';

class CompanyMainView extends StatefulWidget {
  const CompanyMainView({Key? key}) : super(key: key);

  @override
  State<CompanyMainView> createState() => _CompanyMainViewState();
}

class _CompanyMainViewState extends State<CompanyMainView> {
  var _currentIndex = 0;
  late List<Widget> screens;

  @override
  void initState() {
    screens = const [
      CompanyHomeScreen(),
      RequestsScreen(),
      AddCarScreen(),
      ProfileScreen()
    ];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[_currentIndex],
      bottomNavigationBar: SizedBox(
        height: 115,
        child: Padding(
          padding:
              const EdgeInsets.only(top: 12.0, right: 20, left: 20, bottom: 20),
          child: ClayContainer(
            height: 50,
            borderRadius: 10,
            color: Colors.white,
            spread: 10,
            depth: 40,
            curveType: CurveType.convex,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SalomonBottomBar(
                currentIndex: _currentIndex,
                onTap: (i) => setState(() => _currentIndex = i),
                items: [
                  /// Home
                  SalomonBottomBarItem(
                    icon: const Icon(Icons.home),
                    title: Text(Translations.of(context)!.text("home")),
                    selectedColor: Colors.blue,
                  ),

                  /// Likes
                  SalomonBottomBarItem(
                    icon: const Icon(Icons.list_alt),
                    title: Text(Translations.of(context)!.text("list")),
                    selectedColor: Colors.blue,
                  ),

                  SalomonBottomBarItem(
                    icon: const Icon(Icons.add_box_rounded),
                    title: Text(Translations.of(context)!.text("add")),
                    selectedColor: Colors.blue,
                  ),

                  /// Profile
                  SalomonBottomBarItem(
                    icon: const Icon(Icons.person),
                    title: Text(Translations.of(context)!.text("profile")),
                    selectedColor: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
