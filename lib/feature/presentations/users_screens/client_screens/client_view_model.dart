import 'package:aby_seyara/data/models/terms_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../data/models/car_model.dart';
import '../../../../data/models/request_model.dart';
import '../../../../data/models/user_model.dart';
import '../../../../redux/action_report.dart';
import '../../../../redux/app/app_state.dart';
import '../../../../redux/auth/auth_actions.dart';
import '../../../../redux/client/client_actions.dart';
import '../../../../redux/company/company_actions.dart';

class ClientViewModel {
  final ActionReport? getCarReport;
  final ActionReport? getCompaniesReport;
  final ActionReport? getCarsReport;
  final ActionReport? postRequestReport;
  final ActionReport? removeRequestReport;
  final ActionReport? getRequestsReport;
  final ActionReport? updateRequestReport;
  final ActionReport? getRequestUserReport;
  final ActionReport? getCarOwnerUserReport;

  final UserModel? currentUser;

  final Function()? signOut;
  final Function()? getClients;
  final List<UserModel>? clients;
  final List<CarModel>? cars;
  final CarModel? car;
  // final Product? orderProduct;
  final Function(String)? getTerms;
  final TermsModel? terms;

  final Function()? getCars;
  final Function()? getCompanies;
  final List<UserModel>? companies;
  final UserModel? company;

  final Function(String)? getProduct;

  final Function(RequestModel)? postRequest;
  final Function(RequestModel)? getRequestUser;
  final Function(CarModel)? getCarOwnerUser;
  final Function(RequestModel)? getCarInRequest;
  final Function(RequestModel)? removeRequest;
  final Function()? getRequests;
  final Function(String, String)? updateRequest;
  final UserModel? requestUser;
  final UserModel? carOwnerUser;

  final RequestModel? request;
  final List<RequestModel>? requests;

  ClientViewModel({
    this.getCarReport,
    this.getCarsReport,
    this.getCompaniesReport,
    this.postRequestReport,
    this.removeRequestReport,
    this.getRequestsReport,
    this.updateRequestReport,
    this.getRequestUserReport,
    this.getCarOwnerUserReport,
    this.getCarOwnerUser,
    this.carOwnerUser,
    this.currentUser,
    this.signOut,
    this.cars,
    this.car,
    this.requestUser,
    this.companies,
    this.getClients,
    this.clients,
    this.company,
    this.getCars,
    this.getCompanies,
    this.getProduct,
    this.postRequest,
    this.getRequestUser,
    this.getCarInRequest,
    this.removeRequest,
    this.getRequests,
    this.updateRequest,
    this.request,
    this.requests,
    this.getTerms,
    this.terms,
  });
  static ClientViewModel fromStore(Store<AppState>? store) {
    return ClientViewModel(
      currentUser: store!.state.authState?.user,
      requestUser: store.state.clientState?.requestUser,
      carOwnerUser: store.state.clientState?.carOwnerUser,
      terms: store.state.companyState!.terms,
      cars: store.state.companyState?.cars.values.toList(),
      car: store.state.companyState?.car,
      companies: store.state.authState!.companies!.values.toList(),
      company: store.state.authState!.company,
      clients: store.state.clientState!.clients!.values.toList(),
      getClients: () {
        store.dispatch(GetClientsAction());
      },
      getTerms: (type) => store.dispatch(GetTermsAction(type: type)),

      getCars: () => store.dispatch(GetCarsAction()),
      getCompanies: () => store.dispatch(GetCompaniesAction()),
      getProduct: (carId) {
        store.dispatch(GetCarAction(carId: carId));
      },
      // orderProduct: store.state.orderState?.orderProduct,
      request: store.state.clientState?.request,
      requests: store.state.clientState?.requests.values.toList(),
      postRequest: (request) {
        store.dispatch(PostRequestAction(
          request: request,
        ));
      },
      getRequestUser: (request) {
        store.dispatch(GetRequestUserAction(request: request));
      },
      getCarOwnerUser: (car) {
        store.dispatch(GetCarOwnerCompanyAction(car: car));
      },

      // getCarInRequest: (order) {
      //   store.dispatch(GetRequestCarAction(car: order));
      // },
      removeRequest: (request) {
        store.dispatch(RemoveRequestAction(request: request));
      },
      getRequests: () {
        store.dispatch(GetRequestsAction());
      },
      updateRequest: (requestId, status) {
        store.dispatch(
            UpdateRequestAction(requestId: requestId, status: status));
      },
      signOut: () async {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        // ignore: unused_local_variable
        bool? isLogin = preferences.getBool("isLogin") ?? false;
        preferences.setBool("isLogin", false);
        // preferences.remove('users');
        // preferences.clear();
        await FirebaseAuth.instance.signOut();
      },

      getCarReport: store.state.companyState?.status['GetCarAction'],
      getCarsReport: store.state.companyState?.status['GetCarsAction'],
      getCompaniesReport: store.state.authState!.status!['GetCompaniesAction'],
      postRequestReport: store.state.clientState?.status['PostRequestAction'],
      removeRequestReport:
          store.state.clientState?.status['RemoveRequestAction'],
      getRequestsReport: store.state.clientState?.status['GetRequestsAction'],
      updateRequestReport:
          store.state.clientState?.status['UpdateRequestAction'],

      getRequestUserReport:
          store.state.clientState?.status['GetRequestUserAction'],
      getCarOwnerUserReport:
          store.state.clientState?.status['GetCarOwnerCompanyAction'],
    );
  }
}
