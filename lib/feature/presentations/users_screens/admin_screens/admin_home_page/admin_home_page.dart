import 'package:flutter/material.dart';

import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../widgets/default_button.dart';
import '../../../../widgets/search_bar.dart';
import '../add_terms/add_terms.dart';
import 'components/counter_users_widgets/build_counter_users_widget.dart';
import 'components/custom_header.dart';

class AdminHomePageContent extends StatefulWidget {
  const AdminHomePageContent({Key? key}) : super(key: key);

  @override
  State<AdminHomePageContent> createState() => _AdminHomePageContentState();
}

class _AdminHomePageContentState extends State<AdminHomePageContent> {
  final controller = ScrollController();
  double offset = 0;

  @override
  void initState() {
    super.initState();
    controller.addListener(onScroll);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      body: SingleChildScrollView(
        controller: controller,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CustomHeader(
              image: "assets/images/Admin.png",
              textTop: Translations.of(context)!.text("know_all"),
              textBottom: Translations.of(context)!.text("about_app"),
              offset: offset,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              child: SearchBar(),
            ),
            const BuildCounterUsersWidget(),
            Padding(
              padding: const EdgeInsets.only(top: 30, right: 30, left: 30),
              child: DefaultButton(
                text: Translations.of(context)!.text("add_terms"),
                press: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => const AddTermsAndConditions()));
                },
              ),
            ),
            const SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
