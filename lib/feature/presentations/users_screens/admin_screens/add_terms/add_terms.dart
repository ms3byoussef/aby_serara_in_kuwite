import 'package:aby_seyara/data/models/terms_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../../utils/validator.dart';
import '../../../../widgets/custom_form_field.dart';
import '../../../../widgets/default_button.dart';
import '../../company_screens/company_view_model.dart';
import '../admin_main_view.dart';

class AddTermsAndConditions extends StatelessWidget {
  const AddTermsAndConditions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CompanyViewModel>(
      builder: (_, viewModel) => _AddTermsAndConditions(
        viewModel: viewModel,
      ),
      converter: (store) {
        return CompanyViewModel.fromStore(store);
      },
    );
  }
}

class _AddTermsAndConditions extends StatefulWidget {
  final CompanyViewModel viewModel;
  const _AddTermsAndConditions({required this.viewModel});

  @override
  State<_AddTermsAndConditions> createState() => _AddTermsAndConditionsState();
}

class _AddTermsAndConditionsState extends State<_AddTermsAndConditions> {
  TextEditingController termsContentEn = TextEditingController();
  TextEditingController termsContentAr = TextEditingController();
  List<String> termsType = ["Buy", "Pricing", "Test Drive"];
  String? _selectedTermsType;
  ProgressDialog? progressDialog;

  @override
  void didUpdateWidget(_AddTermsAndConditions oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel.postTermsReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            Translations.of(context)!.text("add_terms"),
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel.postTermsReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context,
          CustomSnackBar.error(
            message: widget.viewModel.postTermsReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel.postTermsReport?.status ==
          ActionStatus.complete) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const AdminMainView(),
        ));
        widget.viewModel.postTermsReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AdminMainView(),
                      ));
                    },
                    icon: const Icon(Icons.arrow_back_ios_new)),
              ),
              Text(Translations.of(context)!.text("add_terms"),
                  style: AppTheme.introHeadline),
              CustomFormField(
                maxLine: true,
                controller: termsContentEn,
                label: Translations.of(context)!.text("terms_en"),
                labelTextStyle: AppTheme.hintText.copyWith(color: Colors.black),
                hintText: Translations.of(context)!.text("terms_en"),
                keyboardType: TextInputType.multiline,
                validator: (value) => Validator.validateEmpty(value!),
              ),
              CustomFormField(
                maxLine: true,
                controller: termsContentAr,
                label: Translations.of(context)!.text("terms_ar"),
                labelTextStyle: AppTheme.hintText.copyWith(color: Colors.black),
                hintText: Translations.of(context)!.text("terms_ar"),
                keyboardType: TextInputType.multiline,
                validator: (value) => Validator.validateEmpty(value!),
              ),
              const SizedBox(height: 20),
              Text(Translations.of(context)!.text("type_terms"),
                  style: AppTheme.introHeadline),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 25),
                child: DropdownButtonFormField(
                  validator: (value) => value == null ? 'field required' : null,
                  hint: Text(
                    Translations.of(context)!.text("choose _type"),
                  ), // Not necessary for Option 1
                  value: _selectedTermsType,
                  onChanged: (newValue) {
                    setState(() {
                      _selectedTermsType = newValue.toString();
                    });
                  },
                  items: termsType.map((requestCategory) {
                    return DropdownMenuItem(
                      value: requestCategory,
                      child: Text(requestCategory),
                    );
                  }).toList(),
                ),
              ),
              const SizedBox(height: 20),
              DefaultButton(
                text: Translations.of(context)!.text("add_terms"),
                press: () {
                  TermsModel termsModel = TermsModel(
                      termsContentAr: termsContentAr.text,
                      termsContentEn: termsContentEn.text,
                      type: _selectedTermsType);
                  widget.viewModel.postTerms!(termsModel);
                },
              ),
            ],
          ),
        ),
      )),
    );
  }
}
