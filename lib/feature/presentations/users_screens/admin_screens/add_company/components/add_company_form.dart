// ignore_for_file: library_private_types_in_public_api, empty_catches

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../../data/models/user_model.dart';
import '../../../../../../redux/action_report.dart';
import '../../../../../../redux/app/app_state.dart';
import '../../../../../../trans/translations.dart';
import '../../../../../../utils/progress_dialog.dart';
import '../../../../../../utils/validator.dart';
import '../../../../../widgets/add_image_widgets.dart';
import '../../../../../widgets/custom_form_field.dart';
import '../../../../../widgets/default_button.dart';
import '../../../../../widgets/password_form_field.dart';
import '../../../../auth/auth_view_model.dart';
import '../../admin_main_view.dart';

class AddCompanyForm extends StatelessWidget {
  const AddCompanyForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      builder: (_, viewModel) => AddCompanyFormContent(
        viewModel: viewModel,
      ),
      converter: (store) {
        return AuthViewModel.fromStore(store);
      },
    );
  }
}

class AddCompanyFormContent extends StatefulWidget {
  final AuthViewModel? viewModel;

  const AddCompanyFormContent({
    Key? key,
    this.viewModel,
  }) : super(key: key);
  @override
  _AddCompanyFormContentState createState() => _AddCompanyFormContentState();
}

class _AddCompanyFormContentState extends State<AddCompanyFormContent> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  ProgressDialog? progressDialog;
  File? companyImage;
  final ImagePicker _picker = ImagePicker();
  Future pickImage() async {
    try {
      final image = await _picker.pickImage(source: ImageSource.gallery);
      if (image == null) return; // Capture a photo

      setState(() => companyImage = File(image.path));
      // ignore: unused_catch_clause
    } on PlatformException catch (e) {}
  }

  add() async {
    if (name.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_name_empty"),
        ),
      );
      return;
    }
    if (email.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_email_empty"),
        ),
      );
      return;
    }
    if (phone.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_phone_empty"),
        ),
      );
      return;
    }

    if (password.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_password_empty"),
        ),
      );
      return;
    }

    if (password.text.length < 5) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_password_min_six"),
        ),
      );
      return;
    }

    UserModel company = UserModel(
      role: "company",
      address: "",
      name: name.text,
      email: email.text,
      phone: phone.text,
    );
    widget.viewModel!.addCompany!(company, password.text, companyImage!);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(AddCompanyFormContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel!.addCompanyReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            Translations.of(context)!.text("loading_add_company"),
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel!.addCompanyReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context,
          CustomSnackBar.error(
            message: widget.viewModel!.addCompanyReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel!.addCompanyReport?.status ==
          ActionStatus.complete) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const AdminMainView(),
        ));
        widget.viewModel!.addCompanyReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AddImageWidget(
          imageFile: companyImage,
          title: Translations.of(context)!.text("add_company_image"),
          onTap: pickImage,
        ),
        Form(
          key: _formKey,
          child: Column(
            children: [
              CustomFormField(
                controller: name,
                label: Translations.of(context)!.text("name"),
                hintText: Translations.of(context)!.text("hint_name"),
                keyboardType: TextInputType.name,
                validator: (value) => Validator.validateEmpty(value!),
                prefixIcon: const Icon(
                  Icons.person,
                  color: Colors.white,
                ),
              ),
              CustomFormField(
                controller: email,
                label: Translations.of(context)!.text("email"),
                hintText: Translations.of(context)!.text("hint_email"),
                keyboardType: TextInputType.emailAddress,
                validator: (value) => Validator.validateEmail(value!),
                prefixIcon: const Icon(
                  Icons.email,
                  color: Colors.white,
                ),
              ),
              CustomFormField(
                controller: phone,
                label: Translations.of(context)!.text("phone"),
                hintText: Translations.of(context)!.text("hint_phone"),
                keyboardType: TextInputType.phone,
                validator: (value) => Validator.validateNineNumber(value!),
                prefixIcon: const Icon(
                  Icons.phone,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 10),
              PasswordFormField(
                controller: password,
                hintText: Translations.of(context)!.text("password"),
                label: Translations.of(context)!.text("hint_password"),
                validator: (value) => Validator.validatePassword(value!),
                prefixIcon: const Icon(
                  Icons.lock,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 20),
              DefaultButton(
                text: Translations.of(context)!.text("add_company"),
                press: add,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
