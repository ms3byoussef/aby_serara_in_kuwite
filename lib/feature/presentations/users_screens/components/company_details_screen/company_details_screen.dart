import 'package:aby_seyara/data/models/user_model.dart';
import 'package:aby_seyara/feature/presentations/users_screens/client_screens/client_view_model.dart';
import 'package:aby_seyara/redux/action_report.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../widgets/car_card_in_company/car_card_in_company.dart';

class CompanyDetailsScreen extends StatelessWidget {
  final UserModel company;
  const CompanyDetailsScreen({Key? key, required this.company})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _CompanyDetailsScreen(
        viewModel: viewModel,
        company: company,
      ),
    );
  }
}

class _CompanyDetailsScreen extends StatefulWidget {
  final ClientViewModel viewModel;
  final UserModel company;

  const _CompanyDetailsScreen({required this.viewModel, required this.company});

  @override
  State<_CompanyDetailsScreen> createState() => _CompanyDetailsScreenState();
}

class _CompanyDetailsScreenState extends State<_CompanyDetailsScreen> {
  buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 320,
      stretch: true,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(widget.company.name!,
            style: AppTheme.whiteText.copyWith(fontSize: 20)),
        background: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
            gradient: LinearGradient(
              colors: [Color(0xff3c80f7), Color(0xff1058d1)],
              stops: [0, 1],
              begin: Alignment(-1.00, 0.00),
              end: Alignment(1.00, -0.00),
              // angle: 90,
              // scale: undefined,
            ),
          ),
          child: Hero(
            tag: widget.company.id!,
            child: Image.network(
              widget.company.photo!,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          buildSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                const SizedBox(
                  height: 30,
                ),
                ////////////////////////
                SizedBox(
                  height: MediaQuery.of(context).size.height * .8,
                  child: widget.viewModel.getCarsReport?.status ==
                          ActionStatus.running
                      ? const Center(child: CircularProgressIndicator())
                      : widget.viewModel.cars!.isEmpty
                          ? Center(
                              child: Text(Translations.of(context)!
                                  .text("error_car_empty")))
                          : ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: widget.viewModel.cars!.length,
                              itemBuilder: (BuildContext context, int index) {
                                return (widget
                                            .viewModel.cars![index].companyID ==
                                        widget.company.id)
                                    ? CarCardInCompany(
                                        textColor: AppTheme.primaryColor,
                                        car: widget.viewModel.cars![index],
                                        companyImage: widget.company.photo,
                                      )
                                    : const SizedBox();
                              },
                            ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
