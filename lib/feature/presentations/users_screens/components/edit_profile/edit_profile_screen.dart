import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../../settings/settings_option.dart';
import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../onboarding_screen/welcome_screen/welcome_screen.dart';
import 'components/edit_profile_form.dart';

class EditProfileScreen extends StatefulWidget {
  final SettingsOptions options;
  final ValueChanged<SettingsOptions> onOptionsChanged;
  const EditProfileScreen(
      {Key? key, required this.options, required this.onOptionsChanged})
      : super(key: key);

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: size.height * 1.4,
              width: double.infinity,
              color: AppTheme.primaryColor,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 10, top: 10),
                    height: size.height * .14,
                    width: double.infinity,
                    color: AppTheme.primaryColor,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            LanguageItem(
                                widget.options, widget.onOptionsChanged),
                            Container(),
                            GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(context, "/profile");
                              },
                              child: Container(
                                width: 60,
                                height: 40,
                                padding: const EdgeInsets.all(10),
                                decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  ),
                                ),
                                child: SvgPicture.asset(
                                    "assets/icons/Icon-cross.svg"),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: size.height * 1.2,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: size.height * .05,
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                              bottom:
                                  4, // This can be the space you need between text and underline
                            ),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                              color: AppTheme.primaryColor,
                              width:
                                  2, // This would be the width of the underline
                            ))),
                            child: Text(
                                Translations.of(context)!.text("edit_profile"),
                                style: AppTheme.introHeadline),
                          ),
                          const SizedBox(height: 20),
                          const EditProfileForm(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
