// ignore_for_file: library_private_types_in_public_api, empty_catches, unnecessary_null_comparison

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../../data/models/user_model.dart';
import '../../../../../../redux/action_report.dart';
import '../../../../../../redux/app/app_state.dart';
import '../../../../../../trans/translations.dart';
import '../../../../../../utils/progress_dialog.dart';
import '../../../../../../utils/validator.dart';
import '../../../../../widgets/add_image_widgets.dart';
import '../../../../../widgets/custom_form_field.dart';
import '../../../../../widgets/default_button.dart';
import '../../../../auth/auth_view_model.dart';
import '../../../admin_screens/admin_main_view.dart';
import '../../../client_screens/client_main_view.dart';
import '../../../company_screens/company_main_view.dart';
import '../../map_screen/map_screen.dart';

class EditProfileForm extends StatelessWidget {
  const EditProfileForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      distinct: true,
      converter: (store) => AuthViewModel.fromStore(store),
      builder: (_, viewModel) => EditProfileFormContent(
        viewModel: viewModel,
      ),
    );
  }
}

class EditProfileFormContent extends StatefulWidget {
  final AuthViewModel? viewModel;
  const EditProfileFormContent({Key? key, this.viewModel}) : super(key: key);

  @override
  _EditProfileFormContentState createState() => _EditProfileFormContentState();
}

class _EditProfileFormContentState extends State<EditProfileFormContent> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController address = TextEditingController();

  ProgressDialog? progressDialog;
  final ImagePicker _picker = ImagePicker();
  File? userImage;

  Future pickImage() async {
    try {
      final image = await _picker.pickImage(
          source: ImageSource.gallery,
          preferredCameraDevice: CameraDevice.rear);
      if (image == null) return;

      setState(() => userImage = File(image.path));
      // ignore: unused_catch_clause
    } on PlatformException catch (e) {}
  }

  @override
  void initState() {
    name.value =
        TextEditingValue(text: widget.viewModel!.user!.name.toString());
    email.value =
        TextEditingValue(text: widget.viewModel!.user!.email.toString());
    phone.value =
        TextEditingValue(text: widget.viewModel!.user!.phone.toString());
    address.value =
        TextEditingValue(text: widget.viewModel!.user!.address.toString());
    super.initState();
  }

  @override
  void didUpdateWidget(EditProfileFormContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(
      Duration.zero,
      () {
        if (widget.viewModel!.getUpdateProfileReport?.status ==
            ActionStatus.running) {
          progressDialog ??= ProgressDialog(context);
          if (!progressDialog!.isShowing()) {
            progressDialog!.setMessage(
              Translations.of(context)!.text("loading_edit_profile"),
            );
            progressDialog!.show();
          }
        } else if (widget.viewModel!.getUpdateProfileReport?.status ==
            ActionStatus.error) {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
          showTopSnackBar(
            context,
            CustomSnackBar.error(
              message: widget.viewModel!.getUpdateProfileReport!.msg.toString(),
            ),
          );
          widget.viewModel!.getUpdateProfileReport?.status = null;
        } else if (widget.viewModel!.getUpdateProfileReport?.status ==
            ActionStatus.complete) {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }

          if (widget.viewModel!.user!.role == 'admin') {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (_) => const AdminMainView()),
                (route) => false);
          } else if (widget.viewModel!.user!.role == 'client') {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (_) => const ClientMainView()),
                (route) => false);
          } else {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (_) => const CompanyMainView()),
                (route) => false);
          }
          widget.viewModel!.getUpdateProfileReport?.status = null;
        } else {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
        }
        widget.viewModel!.getUpdateProfileReport?.status = null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        (widget.viewModel!.user!.photo == null ||
                widget.viewModel!.user!.photo == "")
            ? AddImageWidget(
                imageFile: userImage,
                onTap: pickImage,
                title: Translations.of(context)!.text("add_image"),
              )
            : Container(
                height: 140,
                width: 160,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                decoration: const BoxDecoration(
                    shape: BoxShape.circle, color: Colors.white),
                child: InkWell(
                    onTap: pickImage,
                    child: Image.network(
                        widget.viewModel!.user!.photo.toString())),
              ),
        Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                CustomFormField(
                  controller: name,
                  label: Translations.of(context)!.text("name"),
                  hintText: Translations.of(context)!.text("hint_name"),
                  keyboardType: TextInputType.name,
                  validator: (value) => Validator.validateEmpty(value!),
                  prefixIcon: const Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
                CustomFormField(
                  controller: email,
                  readOnly: true,
                  label: Translations.of(context)!.text("email"),
                  hintText: Translations.of(context)!.text("hint_Email"),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) => Validator.validateEmail(value!),
                  prefixIcon: const Icon(
                    Icons.email_rounded,
                    color: Colors.white,
                  ),
                ),
                CustomFormField(
                  controller: phone,
                  label: Translations.of(context)!.text("Phone"),
                  hintText: Translations.of(context)!.text("hint_phone"),
                  keyboardType: TextInputType.phone,
                  validator: (value) => Validator.validateNineNumber(value!),
                  prefixIcon: const Icon(
                    Icons.phone,
                    color: Colors.white,
                  ),
                ),
                CustomFormField(
                  suffixIcon: IconButton(
                    onPressed: () async {
                      final result = await Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const MapScreen()),
                      );
                      if (result != null) {
                        address.text = result;
                      }
                    },
                    icon: const Icon(
                      Icons.location_on_sharp,
                      color: Colors.white,
                    ),
                  ),
                  controller: address,
                  label: Translations.of(context)!.text("address"),
                  hintText: Translations.of(context)!.text("hint_address_name"),
                  validator: (value) => Validator.validateEmpty(value!),
                  keyboardType: TextInputType.streetAddress,
                  prefixIcon: const SizedBox(width: 3),
                ),
                const SizedBox(height: 30),
                DefaultButton(
                  text: Translations.of(context)!.text("edit_profile"),
                  press: () async {
                    if (_formKey.currentState!.validate()) {
                      UserModel userModel = UserModel(
                        id: widget.viewModel!.user!.id,
                        name: name.text,
                        email: email.text,
                        phone: phone.text,
                        address: address.text,
                        role: widget.viewModel!.user!.role,
                      );
                      userImage == null
                          ? widget.viewModel!.updateProfile!(userModel)
                          : await widget.viewModel!.updateProfile!(userModel,
                              img: userImage!);
                    } else {}
                  },
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
