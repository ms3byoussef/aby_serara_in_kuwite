import 'package:aby_seyara/feature/presentations/users_screens/client_screens/client_view_model.dart';
import 'package:aby_seyara/feature/presentations/users_screens/components/all_companies/all_companies.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../widgets/car_card_widget/car_card.dart';
import '../../../../widgets/company_card.dart';
import '../../../../widgets/search_bar.dart';
import '../search_screen/search_screen.dart';

class CarsScreen extends StatelessWidget {
  const CarsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _CarsScreen(
        viewModel: viewModel,
      ),
    );
  }
}

class _CarsScreen extends StatefulWidget {
  final ClientViewModel viewModel;
  const _CarsScreen({Key? key, required this.viewModel}) : super(key: key);

  @override
  State<_CarsScreen> createState() => _CarsScreenState();
}

class _CarsScreenState extends State<_CarsScreen> {
  @override
  void initState() {
    widget.viewModel.getCars!();
    widget.viewModel.getCompanies!();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: size.height * 1.15,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xff3c80f7), Color(0xff1058d1)],
                  stops: [0, 1],
                ),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 50,
                  ),
                  Container(
                    height: size.height * 1.06,
                    decoration: const BoxDecoration(
                      color: Color(0xffeff5ff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0, right: 20, left: 20),
                          child: Container(
                            height: 44,
                            decoration: BoxDecoration(
                              color: const Color(0xffffffff),
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: const [
                                BoxShadow(
                                    color: Color(0x16000000),
                                    offset: Offset(0, 2),
                                    blurRadius: 4,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: const SearchBar(),
                          ),
                        ),

                        const SizedBox(
                          height: 16,
                        ),
                        // Options
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                textAlign: TextAlign.left,
                                Translations.of(context)!.text("Companies"),
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              ),
                              InkWell(
                                onTap: (() => Navigator.of(context)
                                    .pushAndRemoveUntil(
                                        MaterialPageRoute(
                                            builder: (_) =>
                                                const AllCompaniesScreen()),
                                        (route) => false)),
                                child: Text(
                                  textAlign: TextAlign.left,
                                  " ${Translations.of(context)!.text("all_companies")} ...",
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        (widget.viewModel.getCompaniesReport?.status ==
                                ActionStatus.running)
                            ? const Center(child: CircularProgressIndicator())
                            : widget.viewModel.companies!.isEmpty
                                ? Center(
                                    child: Text(Translations.of(context)!
                                        .text("error_company_empty")))
                                : Padding(
                                    padding: const EdgeInsets.only(left: 20),
                                    child: SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              .2,
                                      child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount: widget
                                              .viewModel.companies!.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return CompanyCard(
                                                company: widget.viewModel
                                                    .companies![index]);
                                          }),
                                    ),
                                  ),

                        const SizedBox(
                          height: 30,
                        ),
                        widget.viewModel.currentUser!.role == "client"
                            ? Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 15),
                                    child: Text(
                                      textAlign: TextAlign.left,
                                      Translations.of(context)!
                                          .text("last_news"),
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.all(18),
                                    padding: const EdgeInsets.all(25),
                                    height: size.height * .45,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        image: DecorationImage(
                                          image: const AssetImage(
                                              "assets/images/service.jpg"),
                                          fit: BoxFit.cover,
                                          colorFilter: ColorFilter.mode(
                                              Colors.black.withOpacity(.5),
                                              BlendMode.darken),
                                        )),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Center(
                                          child: Image.asset(
                                            "assets/images/coming.png",
                                            alignment: Alignment.centerRight,
                                            fit: BoxFit.contain,
                                            height: 120,
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () => Navigator.of(context)
                                              .pushAndRemoveUntil(
                                                  MaterialPageRoute(
                                                      builder: (_) =>
                                                          const SearchScreen()),
                                                  (route) => false),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Text(
                                                Translations.of(context)!
                                                    .text("see_all"),
                                                overflow: TextOverflow.fade,
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 25,
                                                    color: Colors.white),
                                              ),
                                              const Icon(
                                                Icons.forward,
                                                color: Colors.white,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  InkWell(
                                    onTap: (() => Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (_) =>
                                                const SearchScreen()))),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      child: Text(
                                        textAlign: TextAlign.left,
                                        " ${Translations.of(context)!.text("all_cars")} ...",
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: size.height * .5,
                                    child: widget.viewModel.getCarsReport
                                                ?.status ==
                                            ActionStatus.running
                                        ? const Center(
                                            child: CircularProgressIndicator())
                                        : widget.viewModel.cars!.isEmpty
                                            ? Center(
                                                child: Text(Translations.of(
                                                        context)!
                                                    .text("error_car_empty")))
                                            : ListView.builder(
                                                scrollDirection: Axis.vertical,
                                                itemCount: widget
                                                    .viewModel.cars!.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return CarCard(
                                                      car: widget.viewModel
                                                          .cars![index]);
                                                },
                                              ),
                                  ),
                                ],
                              ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
