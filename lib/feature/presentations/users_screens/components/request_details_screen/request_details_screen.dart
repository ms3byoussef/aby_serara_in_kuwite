import 'package:aby_seyara/feature/presentations/users_screens/components/car_details_screen/car_details_screen.dart';
import 'package:aby_seyara/feature/widgets/default_button.dart';
import 'package:aby_seyara/utils/app_theme.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:photo_view/photo_view.dart';

import '../../../../../data/models/request_model.dart';
import '../../../../../redux/app/app_state.dart';
import '../../client_screens/client_view_model.dart';
import '../profile/components/account_widget.dart';

class RequestDetailsScreen extends StatelessWidget {
  final RequestModel request;
  const RequestDetailsScreen({Key? key, required this.request})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _RequestDetailsScreen(
        viewModel: viewModel,
        request: request,
      ),
    );
  }
}

class _RequestDetailsScreen extends StatefulWidget {
  final RequestModel request;
  final ClientViewModel viewModel;
  const _RequestDetailsScreen({
    Key? key,
    required this.viewModel,
    required this.request,
  }) : super(key: key);

  @override
  State<_RequestDetailsScreen> createState() => _RequestDetailsScreenState();
}

class _RequestDetailsScreenState extends State<_RequestDetailsScreen> {
  buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 300,
      pinned: true,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        title: Text("${widget.request.type!} Request",
            style: AppTheme.whiteText.copyWith(fontSize: 20)),
        background: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(31),
            gradient: const LinearGradient(
              colors: [Color(0xff3c80f7), Color(0xff1058d1)],
              stops: [0, 1],
              begin: Alignment(-1.00, 0.00),
              end: Alignment(1.00, -0.00),
              // angle: 90,
              // scale: undefined,
            ),
          ),
          child: Hero(
              tag: widget.request.car!.id,
              child: CarouselSlider(
                items: widget.request.car!.carImage!
                    .map((e) => PhotoView(
                          backgroundDecoration:
                              const BoxDecoration(color: Colors.transparent),
                          imageProvider: NetworkImage(
                            e.toString(),
                          ),
                        ))
                    .toList(),
                options: CarouselOptions(
                  autoPlay: true,
                  enlargeCenterPage: true,
                  viewportFraction: 0.9,
                  height: 200,
                  aspectRatio: 2.0,
                  initialPage: 0,
                ),
              )),
        ),
      ),
    );
  }

  @override
  void initState() {
    // getPdf();
    widget.viewModel.getRequestUser!(widget.request);
    widget.viewModel.getCarOwnerUser!(widget.request.car!);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          buildSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                const SizedBox(height: 20),
                widget.viewModel.requestUser != null
                    ? Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 20),
                        margin: const EdgeInsets.only(bottom: 20),
                        height: 200,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(11)),
                            color: Color(0x1a009444)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AccountWidget(
                              title: "User Name",
                              text:
                                  widget.viewModel.requestUser!.name.toString(),
                            ),
                            const Divider(
                              color: Color(0xffababab),
                            ),
                            AccountWidget(
                              title: "User Email",
                              text: widget.viewModel.requestUser!.email
                                  .toString(),
                            ),
                            const Divider(
                              color: Color(0xffababab),
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: AccountWidget(
                                title: "User Phone",
                                text: widget.viewModel.requestUser!.phone
                                    .toString(),
                              ),
                            ),
                          ],
                        ),
                      )
                    : const Center(child: CircularProgressIndicator()),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: DefaultButton(
                      text: "See Car Details",
                      press: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) =>
                                  CarDetailsScreen(car: widget.request.car!)))),
                ),
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("PDF File"),
                ),
                widget.request.agreementFile != null
                    ? SizedBox(
                        height: MediaQuery.of(context).size.height * .8,
                        child: const PDF(
                          swipeHorizontal: true,
                        ).cachedFromUrl(widget.request.agreementFile!),
                      )
                    : const Center(
                        child: CircularProgressIndicator(),
                      ),
                const SizedBox(height: 10),
              ],
            ),
          )
        ],
      ),
    );
  }
}
