import 'package:aby_seyara/feature/presentations/users_screens/company_screens/company_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';
import 'package:searchable_listview/searchable_listview.dart';

import '../../../../../data/models/car_model.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../widgets/car_card_widget/car_card.dart';
import '../../admin_screens/admin_main_view.dart';
import '../../client_screens/client_main_view.dart';
import '../../company_screens/company_main_view.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CompanyViewModel>(
      builder: (_, viewModel) => _SearchScreen(
        viewModel: viewModel,
      ),
      converter: (store) {
        return CompanyViewModel.fromStore(store);
      },
    );
  }
}

class _SearchScreen extends StatefulWidget {
  final CompanyViewModel? viewModel;
  const _SearchScreen({Key? key, this.viewModel}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<_SearchScreen> {
  TextEditingController search = TextEditingController();
  String? searchWord = "";
  ProgressDialog? progressDialog;

  @override
  void initState() {
    super.initState();
    widget.viewModel!.getCars!();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 30),
              width: double.infinity,
              height: MediaQuery.of(context).size.height * .15,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(44),
                    bottomRight: Radius.circular(44)),
                color: AppTheme.primaryColor,
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          Translations.of(context)!.text("all_cars"),
                          style: AppTheme.introHeadline.copyWith(fontSize: 30),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (widget.viewModel!.user!.role == 'admin') {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (_) => const AdminMainView()),
                                (route) => false);
                          } else if (widget.viewModel!.user!.role == 'client') {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (_) => const ClientMainView()),
                                (route) => false);
                          } else {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (_) => const CompanyMainView()),
                                (route) => false);
                          }
                        },
                        child: Container(
                          width: 60,
                          height: 40,
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                            ),
                          ),
                          child:
                              SvgPicture.asset("assets/icons/Icon-cross.svg"),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              height: MediaQuery.of(context).size.height * .8,
              child: SearchableList<CarModel>.sliver(
                initialList: widget.viewModel!.cars!,
                builder: (CarModel carModel) => CarCard(car: carModel),
                filter: (value) => widget.viewModel!.cars!
                    .where(
                      (element) => element.title!.toLowerCase().contains(value),
                    )
                    .toList(),
                emptyWidget: const SizedBox(),
                inputDecoration: InputDecoration(
                  labelText: "Search car",
                  labelStyle: const TextStyle(fontSize: 20),
                  fillColor: Colors.white,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Colors.blue,
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
