import 'package:aby_seyara/data/models/car_model.dart';
import 'package:aby_seyara/data/models/request_model.dart';
import 'package:aby_seyara/feature/presentations/users_screens/components/edit_car_screen/edit_car_screen.dart';
import 'package:aby_seyara/utils/app_theme.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../widgets/car_details_card.dart';
import '../../client_screens/client_view_model.dart';
import '../terms_and_conditions/terms_and_conditions.dart';

class CarDetailsScreen extends StatelessWidget {
  final CarModel car;
  const CarDetailsScreen({Key? key, required this.car}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _CarDetailsScreen(
        viewModel: viewModel,
        car: car,
      ),
    );
  }
}

class _CarDetailsScreen extends StatefulWidget {
  final CarModel? car;
  final ClientViewModel viewModel;
  const _CarDetailsScreen({Key? key, this.car, required this.viewModel})
      : super(key: key);

  @override
  State<_CarDetailsScreen> createState() => _CarDetailsScreenState();
}

class _CarDetailsScreenState extends State<_CarDetailsScreen> {
  List<String> requestType = ["Buy", "Pricing", "Test Drive"];
  String? _selectedRequestsType;
  final List<Color> _selectedAllColors = [];

  postRequest() async {
    if (_selectedRequestsType == null) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("choose _type"),
        ),
      );
      return;
    }
    RequestModel request = RequestModel(
        car: widget.car,
        userID: widget.viewModel.currentUser!.id,
        status: "waiting",
        orderTime: DateFormat.yMMMEd().format(DateTime.now()).toString(),
        type: _selectedRequestsType);
    await widget.viewModel.getTerms!(request.type!);

    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(30), bottom: Radius.circular(10)),
        ),
        context: context,
        builder: (_) => TermsAndConditions(
              request: request,
            ));
    // widget.viewModel.postRequest!(request);
  }

  buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 300,
      pinned: true,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(widget.car!.title!,
            style: AppTheme.whiteText.copyWith(fontSize: 18)),
        background: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(31),
            gradient: const LinearGradient(
              colors: [Color(0xff3c80f7), Color(0xff1058d1)],
              stops: [0, 1],
              begin: Alignment(-1.00, 0.00),
              end: Alignment(1.00, -0.00),
              // angle: 90,
              // scale: undefined,
            ),
          ),
          child: Hero(
              tag: widget.car!.id,
              child: CarouselSlider(
                items: widget.car!.carImage!
                    .map((e) => PhotoView(
                          backgroundDecoration:
                              const BoxDecoration(color: Colors.transparent),
                          imageProvider: NetworkImage(
                            e.toString(),
                          ),
                        ))
                    .toList(),
                options: CarouselOptions(
                  autoPlay: true,
                  enlargeCenterPage: true,
                  viewportFraction: 0.9,
                  height: 200,
                  aspectRatio: 2.0,
                  initialPage: 0,
                ),
              )),
        ),
      ),
    );
  }

  priceAndBookNow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: const EdgeInsets.only(left: 30),
          child: RichText(
            text: TextSpan(
              style: const TextStyle(
                  fontSize: 20,
                  color: Colors.black87,
                  fontWeight: FontWeight.w500),
              children: <TextSpan>[
                TextSpan(text: widget.car!.price!),
                TextSpan(
                    text: '/${Translations.of(context)!.text("kwd")}',
                    style: const TextStyle(color: Colors.black38)),
              ],
            ),
          ),
        ),
        SizedBox(
          width: 200,
          height: 75,
          child: widget.viewModel.currentUser!.role == 'client'
              ? ElevatedButton(
                  onPressed: postRequest,
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: AppTheme.primaryColor,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                      ),
                    ),
                  ),
                  child: Text(
                    _selectedRequestsType == null
                        ? Translations.of(context)!.text("request")
                        : _selectedRequestsType == "Test Drive"
                            ? Translations.of(context)!.text("drive_request")
                            : _selectedRequestsType == "Pricing"
                                ? Translations.of(context)!
                                    .text("pricing_request")
                                : _selectedRequestsType == "Buy"
                                    ? Translations.of(context)!
                                        .text("sell_request")
                                    : "",
                    style: AppTheme.introHeadline.copyWith(color: Colors.white),
                  ),
                )
              : ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => EditCarScreen(car: widget.car),
                    ));
                  },
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: AppTheme.primaryColor,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                      ),
                    ),
                  ),
                  child: Text(
                    Translations.of(context)!.text("Edit_car"),
                    style: AppTheme.introHeadline.copyWith(color: Colors.white),
                  ),
                ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          buildSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                const SizedBox(height: 20),
                Wrap(
                  children: [
                    CarDetailsCard(
                        title: widget.car!.carDetails!.brand!,
                        iconsOrText: Image.asset(
                          "assets/icons/brand.png",
                          width: 50,
                        )),
                    CarDetailsCard(
                        title: widget.car!.carDetails!.modelBody!,
                        iconsOrText: Image.asset(
                          "assets/icons/modelBody.png",
                          width: 40,
                        )),
                    CarDetailsCard(
                        title: widget.car!.carDetails!.modelYear!,
                        iconsOrText: Image.asset(
                          "assets/icons/modelYear.png",
                          width: 50,
                        )),
                    CarDetailsCard(
                        title: widget.car!.carDetails!.engine!,
                        iconsOrText: Image.asset(
                          "assets/icons/engine.png",
                          width: 50,
                        )),
                    CarDetailsCard(
                      title: widget.car!.carDetails!.transmission!,
                      iconsOrText: Image.asset(
                        "assets/icons/transmission.png",
                        width: 50,
                      ),
                    ),
                    CarDetailsCard(
                      title: widget.car!.carDetails!.numberOfGears!,
                      iconsOrText: Image.asset(
                        "assets/icons/gear.png",
                        width: 50,
                      ),
                    ),
                    CarDetailsCard(
                      title: widget.car!.carDetails!.fuel!,
                      iconsOrText: Image.asset(
                        "assets/icons/fuel.png",
                        width: 50,
                      ),
                    ),
                    CarDetailsCard(
                      title: widget.car!.carDetails!.numberOfAirbag!,
                      iconsOrText: Image.asset(
                        "assets/icons/airbag.png",
                        width: 50,
                      ),
                    ),
                    CarDetailsCard(
                      title: widget.car!.carDetails!.numberOfSeating!,
                      iconsOrText: Image.asset(
                        "assets/icons/noOfSeat.png",
                        width: 40,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(Translations.of(context)!.text("available_color")),
                SizedBox(
                  height: 70,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: widget.car!.carDetails!.colors!.length,
                      itemBuilder: (context, index) {
                        for (var element in widget.car!.carDetails!.colors!) {
                          _selectedAllColors.add(Color(element));
                        }
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircleAvatar(
                            backgroundColor: _selectedAllColors[index],
                            radius: 15,
                          ),
                        );
                      }),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                  child: Text(
                    Translations.of(context)!.text("car_description"),
                    style: AppTheme.introHeadline,
                  ),
                ),
                Text(
                  widget.car!.description ?? "",
                  style: AppTheme.introText,
                ),
                const SizedBox(
                  height: 30,
                ),
                const SizedBox(
                  height: 40,
                ),
                widget.viewModel.currentUser!.role == "client"
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 25, top: 30, right: 20),
                            child: Text(
                              Translations.of(context)!
                                  .text("What_type _of_request"),
                              style: const TextStyle(
                                color: Color(0xff363636),
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 25),
                            child: DropdownButtonFormField(
                              validator: (value) =>
                                  value == null ? 'field required' : null,
                              hint: Text(
                                Translations.of(context)!.text("choose _type"),
                              ), // Not necessary for Option 1
                              value: _selectedRequestsType,
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedRequestsType = newValue.toString();
                                });
                              },
                              items: requestType.map((requestCategory) {
                                return DropdownMenuItem(
                                  value: requestCategory,
                                  child: Text(requestCategory),
                                );
                              }).toList(),
                            ),
                          ),
                        ],
                      )
                    : const SizedBox(),
                const SizedBox(
                  height: 10,
                ),
                priceAndBookNow()
              ],
            ),
          )
        ],
      ),
    );
  }
}
