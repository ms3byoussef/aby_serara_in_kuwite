// ignore_for_file: library_private_types_in_public_api, unused_local_variable

import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import 'package:aby_seyara/data/models/car_model.dart';
import 'package:aby_seyara/feature/presentations/users_screens/company_screens/company_view_model.dart';

import '../../../../../data/models/car_details_model.dart';
import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/constant.dart';
import '../../../../../utils/data_utils.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../../utils/validator.dart';
import '../../../../widgets/custom_drop_down.dart';
import '../../../../widgets/custom_form_field.dart';
import '../../../../widgets/default_button.dart';
import '../../../../widgets/search_drop_down.dart';

class EditCarScreen extends StatelessWidget {
  final CarModel? car;
  const EditCarScreen({Key? key, this.car}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CompanyViewModel>(
      builder: (_, viewModel) => EditCarContent(
        viewModel: viewModel,
        car: car,
      ),
      converter: (store) {
        return CompanyViewModel.fromStore(store);
      },
    );
  }
}

class EditCarContent extends StatefulWidget {
  final CarModel? car;
  final CompanyViewModel? viewModel;

  const EditCarContent({Key? key, this.viewModel, this.car}) : super(key: key);

  @override
  _EditCarContentState createState() => _EditCarContentState();
}

class _EditCarContentState extends State<EditCarContent> {
  TextEditingController title = TextEditingController();
  TextEditingController price = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController brand = TextEditingController();
  TextEditingController modelYear = TextEditingController();
  TextEditingController modelBody = TextEditingController();
  TextEditingController fuel = TextEditingController();
  TextEditingController engine = TextEditingController();
  TextEditingController transmission = TextEditingController();
  TextEditingController numberOfAirbag = TextEditingController();
  TextEditingController numberOfGears = TextEditingController();
  TextEditingController numberOfSeating = TextEditingController();

  ProgressDialog? progressDialog;
  final ImagePicker _picker = ImagePicker();
  List<XFile> carImages = [];
  List<String> carImagesUrls = [];
  bool isUpload = false;
  List<int> _selectedIntColors = [];
  final List<Color> _selectedAllColors = [];

  Future pickImage() async {
    carImages.clear();
    carImagesUrls.toList().clear();
    try {
      final images = await _picker.pickMultiImage();
      if (images == null) return; // Capture a photo

      setState(() {
        carImages.addAll(images);
        isUpload = true;
      });
      // ignore: empty_catches
    } on PlatformException {}
  }

  Future<String> uploadImage(XFile? image) async {
    firebase_storage.FirebaseStorage storage =
        firebase_storage.FirebaseStorage.instance;
    firebase_storage.Reference ref =
        storage.ref().child("cars_images").child(image!.name.toString());
    final UploadTask uploadTask = ref.putFile(File(image.path));

    // ignore: avoid_print
    await uploadTask.whenComplete(() => print("complategetDownloadURL"));
    return await ref.getDownloadURL();
  }

  void uploadFunction(List<XFile> images) async {
    for (int i = 0; i < images.length; i++) {
      var imageUrl = await uploadImage(images[i]);
      carImagesUrls.toList().add(imageUrl.toString());
    }
    setState(() {
      isUpload = false;
    });
  }

  @override
  void initState() {
    super.initState();
    carImagesUrls = widget.car!.carImage!;
    title = TextEditingController(text: widget.car!.title);
    price = TextEditingController(text: widget.car!.price);
    description = TextEditingController(text: widget.car!.description);
    brand = TextEditingController(text: widget.car!.carDetails!.brand);
    modelYear = TextEditingController(text: widget.car!.carDetails!.modelYear);
    modelBody = TextEditingController(text: widget.car!.carDetails!.modelBody);
    fuel = TextEditingController(text: widget.car!.carDetails!.fuel);
    engine = TextEditingController(text: widget.car!.carDetails!.engine);
    transmission =
        TextEditingController(text: widget.car!.carDetails!.transmission);
    numberOfAirbag =
        TextEditingController(text: widget.car!.carDetails!.numberOfAirbag);
    numberOfGears =
        TextEditingController(text: widget.car!.carDetails!.numberOfGears);
    numberOfSeating =
        TextEditingController(text: widget.car!.carDetails!.numberOfSeating);

    _selectedIntColors = widget.car!.carDetails!.colors!;
    // ignore: avoid_function_literals_in_foreach_calls
    _selectedIntColors.forEach((element) {
      Color colorFormInt = Color(element);
      _selectedAllColors.add(colorFormInt);
    });
  }

  edit() async {
    if (title.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_car_name_empty"),
        ),
      );
      return;
    }

    if (price.text.isEmpty) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: Translations.of(context)!.text("error_price_empty"),
        ),
      );
      return;
    }
    CarDetailsModel carDetails = CarDetailsModel(
        brand: brand.text,
        engine: engine.text,
        fuel: fuel.text,
        modelBody: modelBody.text,
        modelYear: modelYear.text,
        transmission: transmission.text,
        numberOfAirbag: numberOfAirbag.text,
        numberOfGears: numberOfGears.text,
        numberOfSeating: numberOfSeating.text,
        colors: _selectedIntColors);
    CarModel car = CarModel(
        id: widget.car!.id,
        title: title.text,
        price: price.text,
        description: description.text,
        carDetails: carDetails,
        carImage: widget.car!.carImage,
        companyID: widget.car!.companyID);
    widget.viewModel!.editCar!(car);
  }

  @override
  void didUpdateWidget(EditCarContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel!.editCarReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            Translations.of(context)!.text("loading_edit_car"),
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel!.editCarReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context,
          CustomSnackBar.error(
            message: widget.viewModel!.editCarReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel!.editCarReport?.status ==
          ActionStatus.complete) {
        Navigator.pop(context);

        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        widget.viewModel!.editCarReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          buildSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [buildCarDetailsForm()],
            ),
          )
        ],
      ),
    );
  }

  buildCarDetailsForm() {
    final size = MediaQuery.of(context).size;

    return Container(
      margin: const EdgeInsets.all(14),
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: size.height * .04,
          ),
          Container(
            padding: const EdgeInsets.only(
              bottom:
                  4, // This can be the space you need between text and underline
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: AppTheme.primaryColor,
                  width: 2, // This would be the width of the underline
                ),
              ),
            ),
            child: Text(Translations.of(context)!.text("add_car_title_price"),
                style: AppTheme.introHeadline),
          ),
          const SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                child: CustomFormField(
                  controller: title,
                  label: Translations.of(context)!.text("car_title"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("car_title")}",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  keyboardType: TextInputType.name,
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomFormField(
                  controller: price,
                  label: Translations.of(context)!.text("car_price"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("car_price")}",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  keyboardType: TextInputType.number,
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
            ],
          ),
          CustomFormField(
            maxLine: true,
            controller: description,
            label: Translations.of(context)!.text("car_description"),
            labelTextStyle: AppTheme.hintText.copyWith(color: Colors.black),
            hintText:
                "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("car_description")}",
            keyboardType: TextInputType.multiline,
            validator: (value) => Validator.validateEmpty(value!),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18),
            child: Text(Translations.of(context)!.text("car_details"),
                style: AppTheme.introHeadline),
          ),
          Row(
            children: [
              Expanded(
                child: CustomSearchDropDown(
                  label: Translations.of(context)!.text("car_brand"),
                  controller: brand,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("car_details")}",
                  dropDownList: carBrands,
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomFormField(
                  controller: modelYear,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  label: Translations.of(context)!.text("modelYear"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("modelYear")}",
                  keyboardType: TextInputType.number,
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomSearchDropDown(
                  controller: modelBody,
                  label: Translations.of(context)!.text("modelBody"),
                  dropDownList: carBody,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("modelBody")}",
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomDropDown(
                  controller: fuel,
                  label: Translations.of(context)!.text("fuel"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("fuel")}",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  dropDownList: fuelTypes,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomFormField(
                  controller: engine,
                  label: Translations.of(context)!.text("engine"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("engine")}",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  keyboardType: TextInputType.name,
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomDropDown(
                  controller: transmission,
                  label: Translations.of(context)!.text("transmission"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("transmission")}",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  dropDownList: transmissionsTypes,
                ),
              ),
            ],
          ),
          Text(
            Translations.of(context)!.text("color"),
            style: AppTheme.cardText,
          ),
          Row(
            children: [
              ElevatedButton(
                onPressed: () {
                  _selectedAllColors.clear();
                  _selectedIntColors.clear();
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(
                              Translations.of(context)!.text("choice_color")),
                          content: SingleChildScrollView(
                            child: MultipleChoiceBlockPicker(
                              availableColors: carColors,
                              pickerColors: _selectedAllColors, //default color
                              onColorsChanged: (List<Color> colors) {
                                _selectedAllColors == colors;
                              },
                            ),
                          ),
                          actions: <Widget>[
                            ElevatedButton(
                              child: Text(Translations.of(context)!.text("ok")),
                              onPressed: () {
                                for (var element in _selectedAllColors) {
                                  int intColor = convertToInt(element);
                                  _selectedIntColors.add(intColor);
                                }
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      });
                },
                child: Text(Translations.of(context)!.text("choice_color")),
              ),
              const SizedBox(width: 10),
            ],
          ),
          SizedBox(
            height: 70,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: _selectedAllColors.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircleAvatar(
                      backgroundColor: _selectedAllColors[index],
                      radius: 15,
                    ),
                  );
                }),
          ),
          Row(
            children: [
              Expanded(
                child: CustomDropDown(
                  controller: numberOfAirbag,
                  label: Translations.of(context)!.text("number_of_airbag"),
                  hintText: Translations.of(context)!.text("number_of_airbag"),
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  dropDownList: numOf,
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomFormField(
                  controller: numberOfGears,
                  label: Translations.of(context)!.text("number_of_gears"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("number_of_gears")}",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                ),
              ),
            ],
          ),
          Row(
            children: [
              const SizedBox(width: 10),
              Expanded(
                child: CustomDropDown(
                  controller: numberOfSeating,
                  label: Translations.of(context)!.text("number_of_seating"),
                  hintText:
                      "${Translations.of(context)!.text("enter")} ${Translations.of(context)!.text("number_of_seating")}",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  dropDownList: numOf,
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          isUpload == true
              ? const CircularProgressIndicator()
              : DefaultButton(
                  text: Translations.of(context)!.text("Edit_car"),
                  press: edit,
                ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }

  buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 350,
      pinned: true,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(Translations.of(context)!.text("add_car"),
            style: AppTheme.whiteText.copyWith(fontSize: 22)),
        background: Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30),
              ),
              gradient: LinearGradient(
                colors: [Color(0xff3c80f7), Color(0xff1058d1)],
                stops: [0, 1],
                begin: Alignment(-1.00, 0.00),
                end: Alignment(1.00, -0.00),
                // angle: 90,
                // scale: undefined,
              ),
            ),
            child: carImages.isNotEmpty
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 30,
                      ),
                      AddMultiImageWidget(
                          onPressed: pickImage,
                          items: widget.car!.carImage!.isEmpty
                              ? carImages
                                  .map((e) => Image.file(
                                        File(e.path),
                                        fit: BoxFit.cover,
                                        width: 300,
                                        height: 200,
                                      ))
                                  .toList()
                              : widget.car!.carImage!
                                  .map((e) => Image.network(
                                        e,
                                        fit: BoxFit.cover,
                                        width: 300,
                                        height: 200,
                                      ))
                                  .toList()),
                      isUpload == true
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text("Upload images"),
                                IconButton(
                                  icon: const Icon(Icons.upload_sharp),
                                  onPressed: () async {
                                    uploadFunction(carImages);
                                  },
                                ),
                              ],
                            )
                          : const SizedBox(),
                    ],
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: const Icon(Icons.add_a_photo),
                        onPressed: pickImage,
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                    ],
                  )),
      ),
    );
  }
}

class AddMultiImageWidget extends StatelessWidget {
  final List<Widget> items;
  final void Function() onPressed;
  const AddMultiImageWidget(
      {super.key, required this.items, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        items.isNotEmpty
            ? CarouselSlider(
                items: items,
                options: CarouselOptions(
                  autoPlay: true,
                  enlargeCenterPage: true,
                  viewportFraction: 0.9,
                  height: 130,
                  aspectRatio: 2.0,
                  initialPage: 1,
                ),
              )
            : const SizedBox(),
        const SizedBox(height: 20),
        IconButton(
          icon: const Icon(Icons.add_a_photo),
          onPressed: onPressed,
        ),
      ],
    );
  }
}
