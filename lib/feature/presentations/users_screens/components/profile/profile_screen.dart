// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../widgets/default_button.dart';
import '../../../auth/auth_view_model.dart';
import '../../../auth/reset_password/reset_password.dart';
import '../../admin_screens/admin_main_view.dart';
import '../../client_screens/client_main_view.dart';
import '../../company_screens/company_main_view.dart';
import 'components/account_widget.dart';
import 'components/help_widget.dart';
import 'components/icon_headline.dart';
import 'components/profile_alert_dialog.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      builder: (_, viewModel) => ProfileScreenContent(
        viewModel: viewModel,
      ),
      converter: (store) {
        return AuthViewModel.fromStore(store);
      },
    );
  }
}

class ProfileScreenContent extends StatefulWidget {
  final AuthViewModel? viewModel;

  const ProfileScreenContent({this.viewModel, Key? key}) : super(key: key);

  @override
  _ProfileScreenContentState createState() => _ProfileScreenContentState();
}

class _ProfileScreenContentState extends State<ProfileScreenContent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconHeadline(
                      title: Translations.of(context)!.text("account"),
                      hint: Translations.of(context)!.text("manage_account"),
                      image: "assets/icons/account.svg",
                    ),
                    IconButton(
                        onPressed: () {
                          if (widget.viewModel!.user!.role == 'admin') {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (_) => const AdminMainView()),
                                (route) => false);
                          } else if (widget.viewModel!.user!.role == 'client') {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (_) => const ClientMainView()),
                                (route) => false);
                          } else {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (_) => const CompanyMainView()),
                                (route) => false);
                          }
                        },
                        icon: const Icon(Icons.menu))
                  ],
                ), // Rectangle 1915
                Container(
                  width: double.infinity,
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                  margin: const EdgeInsets.only(bottom: 20),
                  height: 220,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(11)),
                      color: Color(0x1a009444)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      AccountWidget(
                          title: Translations.of(context)!.text("email"),
                          text: widget.viewModel!.user!.email.toString()),
                      const Divider(
                        color: Color(0xffababab),
                      ),
                      AccountWidget(
                          title: Translations.of(context)!.text("phone"),
                          text: widget.viewModel!.user!.phone.toString()),
                      const Divider(
                        color: Color(0xffababab),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: AccountWidget(
                            title: Translations.of(context)!.text("address"),
                            text: widget.viewModel!.user!.address.toString()),
                      ),
                    ],
                  ),
                ),
                widget.viewModel!.user!.role != "admin"
                    ? DefaultButton(
                        text: Translations.of(context)!.text("edit_profile"),
                        press: () {
                          Navigator.pushNamedAndRemoveUntil(
                              context, "/edit_profile", (route) => false);
                        },
                      )
                    : const SizedBox(
                        height: 20,
                      ),
                IconHeadline(
                  title: Translations.of(context)!.text("help"),
                  hint: Translations.of(context)!.text("get_support"),
                  image: "assets/icons/help.svg",
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                  width: double.infinity,
                  margin: const EdgeInsets.only(bottom: 20),
                  height: 140,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(11)),
                      color: Color(0x1a009444)),
                  child: Column(
                    children: [
                      HelpWidget(
                        text: Translations.of(context)!.text("call_us"),
                        press: () {
                          showDialog(
                              context: context,
                              builder: (_) => const ProfileAlertDialog());
                        },
                      ),
                      const Divider(
                        color: Color(0xffababab),
                      ),
                      HelpWidget(
                        text: Translations.of(context)!.text("rest_password"),
                        press: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => const ResetPassword()));
                        },
                      ),
                    ],
                  ),
                ),
                DefaultButton(
                  text: Translations.of(context)!.text("logout"),
                  press: () async {
                    widget.viewModel!.signOut!();
                    Navigator.of(context).pushReplacementNamed("/");
                    setState(() {});
                  },
                ),
                const SizedBox(height: 20)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
