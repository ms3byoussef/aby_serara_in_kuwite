import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../../../utils/app_theme.dart';

class IconHeadline extends StatelessWidget {
  final String? title;
  final String? hint;
  final String? image;
  const IconHeadline({
    this.title,
    this.hint,
    this.image,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          // Rectangle 1916
          Container(
            margin: const EdgeInsets.only(right: 8, left: 8),
            width: 40,
            height: 40,
            child: SvgPicture.asset(
              image!,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // account
              Text(title!,
                  style: AppTheme.cardText.copyWith(fontSize: 20),
                  textAlign: TextAlign.center),
              Text(
                hint!,
                style: AppTheme.hintText,
              )
            ],
          )
        ],
      ),
    );
  }
}
