import 'package:flutter/material.dart';

import '../../../../../../trans/translations.dart';
import '../../../../../widgets/default_button.dart';
import 'account_widget.dart';

class ProfileAlertDialog extends StatelessWidget {
  const ProfileAlertDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: SizedBox(
        height: 130,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AccountWidget(
                title: Translations.of(context)!.text("email"),
                text: "",
              ),
              const Divider(
                color: Color(0xffababab),
              ),
              AccountWidget(
                title: Translations.of(context)!.text("phone"),
                text: "+965 6906 6111",
              ),
            ]),
      ),
      title: Text(
        Translations.of(context)!.text("call_us"),
        style: Theme.of(context).textTheme.headline5,
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 65),
          child: DefaultButton(
            height: 45,
            text: Translations.of(context)!.text("ok"),
            press: () {
              Navigator.pop(context);
            },
          ),
        )
      ],
    );
  }
}
