import 'package:flutter/material.dart';

import '../../../../../../utils/app_theme.dart';

class HelpWidget extends StatelessWidget {
  final String? text;
  final Function? press;
  const HelpWidget({
    this.text,
    this.press,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text!,
          style: AppTheme.cardText
              .copyWith(fontSize: 20, fontWeight: FontWeight.w500),
        ),
        IconButton(
            onPressed: press as void Function()?,
            icon: const Icon(
              Icons.arrow_forward_ios_outlined,
              color: Color(0x66000000),
            ))
      ],
    );
  }
}
