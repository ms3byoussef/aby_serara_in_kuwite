import 'package:flutter/material.dart';

import '../../../../../../utils/app_theme.dart';

class AccountWidget extends StatelessWidget {
  final String? title;
  final String? text;
  const AccountWidget({
    this.title,
    this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title!,
          style: AppTheme.hintText
              .copyWith(fontSize: 16, fontWeight: FontWeight.w400),
        ),
        const SizedBox(height: 4),
        Text(
          text!,
          style: AppTheme.cardText
              .copyWith(fontSize: 18, fontWeight: FontWeight.w400),
        ),
      ],
    );
  }
}
