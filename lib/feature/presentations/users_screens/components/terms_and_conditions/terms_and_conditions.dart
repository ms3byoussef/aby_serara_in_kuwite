// ignore_for_file: avoid_print

import 'dart:io';

import 'package:aby_seyara/data/models/request_model.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../trans/translations.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../widgets/default_button.dart';
import '../../client_screens/client_view_model.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class TermsAndConditions extends StatelessWidget {
  final RequestModel request;
  const TermsAndConditions({Key? key, required this.request}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _TermsAndConditions(
        viewModel: viewModel,
        request: request,
      ),
    );
  }
}

class _TermsAndConditions extends StatefulWidget {
  final RequestModel request;
  final ClientViewModel viewModel;
  const _TermsAndConditions({required this.request, required this.viewModel});

  @override
  State<_TermsAndConditions> createState() => _TermsAndConditionsState();
}

class _TermsAndConditionsState extends State<_TermsAndConditions> {
  ProgressDialog? progressDialog;
  bool agreement = false;
  bool isUpload = false;
  File? pdfFile;

  @override
  void didUpdateWidget(_TermsAndConditions oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel.postRequestReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            Translations.of(context)!.text("request"),
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel.postRequestReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context,
          CustomSnackBar.error(
            message: widget.viewModel.postRequestReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel.postRequestReport?.status ==
          ActionStatus.complete) {
        Navigator.of(context).pop();

        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        widget.viewModel.postRequestReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  uploadFile() async {
    try {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      firebase_storage.Reference ref =
          storage.ref().child("PDF_Files").child(pdfFile.toString());
      final uploadTask = ref.putFile(pdfFile!);

      await uploadTask.whenComplete(() => print("complete"));
      var url = await ref.getDownloadURL();
      setState(() {
        widget.request.agreementFile = url;
        isUpload = true;
      });
      if (pdfFile != null) {
        // ignore: use_build_context_synchronously
        showTopSnackBar(
          context,
          const CustomSnackBar.success(
            message: "File Uploaded",
          ),
        );
      } else {
        // ignore: use_build_context_synchronously
        showTopSnackBar(
          context,
          const CustomSnackBar.error(
            message: "Something want wrong",
          ),
        );
      }
    } on Exception catch (e) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: e.toString(),
        ),
      );
    }
  }

  @override
  void initState() {
    widget.viewModel.getTerms!(widget.request.type!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 25,
                  height: 25,
                  margin: const EdgeInsets.symmetric(vertical: 25),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                  ),
                  child: SvgPicture.asset("assets/icons/Icon-cross.svg",
                      fit: BoxFit.contain),
                ),
              ),
              Text(Translations.of(context)!.text("terms_conditions"),
                  style: AppTheme.introHeadline),
              widget.viewModel.terms != null
                  ? Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                        widget.request.type == null
                            ? Translations.of(context)!.text("request")
                            : widget.request.type == "Test Drive"
                                ? Translations.of(context)!
                                    .text("drive_request")
                                : widget.request.type == "Pricing"
                                    ? Translations.of(context)!
                                        .text("pricing_request")
                                    : widget.request.type == "Buy"
                                        ? Translations.of(context)!
                                            .text("sell_request")
                                        : "",
                        style: AppTheme.introHeadline
                            .copyWith(color: AppTheme.primaryColor),
                      ),
                    )
                  : const CircularProgressIndicator(),
              widget.viewModel.terms != null
                  ? Align(
                      alignment: Alignment.centerRight,
                      child: Directionality(
                        textDirection: TextDirection.rtl,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Text(widget.viewModel.terms!.termsContentAr!,
                              style: AppTheme.introHeadline),
                        ),
                      ),
                    )
                  : const CircularProgressIndicator(),
              widget.viewModel.terms != null
                  ? Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Text(widget.viewModel.terms!.termsContentEn!,
                          style: AppTheme.introHeadline),
                    )
                  : const CircularProgressIndicator(),
              Row(
                children: [
                  Text(Translations.of(context)!.text("agree_terms"),
                      style: AppTheme.introHeadline),
                  Checkbox(
                      value: agreement,
                      activeColor: AppTheme.primaryColor,
                      checkColor: Colors.black,
                      onChanged: ((value) => setState(() {
                            agreement = value!;
                          }))),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Text("Upload file"),
                  IconButton(
                    icon: const Icon(Icons.upload_sharp),
                    onPressed: () async {
                      var result = await FilePicker.platform.pickFiles(
                          allowedExtensions: ['pdf'], type: FileType.custom);
                      if (result != null) {
                        setState(() {
                          pdfFile = File(result.files.single.path.toString());
                        });
                        // await OpenFile.open(result.files.first.path);
                        uploadFile();
                      }
                    },
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                    "${Translations.of(context)!.text("request")} ${widget.request.car!.title}"),
              ),
              isUpload
                  ? Padding(
                      padding: const EdgeInsets.symmetric(vertical: 25),
                      child: DefaultButton(
                        text: Translations.of(context)!.text("request"),
                        press: () async {
                          if (agreement == false) {
                            showTopSnackBar(
                              context,
                              CustomSnackBar.error(
                                message: Translations.of(context)!
                                    .text("agree_terms"),
                              ),
                            );
                            return;
                          }
                          widget.viewModel.postRequest!(
                            widget.request,
                          );
                        },
                      ),
                    )
                  : const CircularProgressIndicator()
            ],
          ),
        ),
      )),
    );
  }
}
