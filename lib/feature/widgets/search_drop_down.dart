// ignore_for_file: must_be_immutable

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

import '../../utils/app_theme.dart';

class CustomSearchDropDown extends StatefulWidget {
  String? label;
  String? hintText;
  TextEditingController? controller = TextEditingController();
  List<String> dropDownList;
  TextStyle? labelTextStyle = AppTheme.whiteText;

  CustomSearchDropDown({
    this.controller,
    this.label,
    this.hintText,
    this.labelTextStyle,
    required this.dropDownList,
    Key? key,
  }) : super(key: key);

  @override
  State<CustomSearchDropDown> createState() => _CustomSearchDropDownState();
}

class _CustomSearchDropDownState extends State<CustomSearchDropDown> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(
          height: 20,
        ),
        Text(widget.label!, style: widget.labelTextStyle),
        const SizedBox(height: 10.0),
        Container(
          height: 55,
          width: double.infinity,
          decoration: kBoxDecorationStyle,
          alignment: Alignment.centerLeft,
          child: DropdownSearch<String>(
              dropdownButtonProps:
                  const DropdownButtonProps(color: Colors.white),
              popupProps: PopupProps.menu(
                showSearchBox: true,
                textStyle: AppTheme.whiteText,
                showSelectedItems: true,
              ),
              items: widget.dropDownList,
              selectedItem: widget.controller!.text,
              onChanged: (String? newValue) {
                setState(() {
                  widget.controller!.text = newValue!;
                  // print(newValue);
                });
              }),
        ),
      ],
    );
  }
}
