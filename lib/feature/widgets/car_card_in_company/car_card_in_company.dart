import 'package:aby_seyara/data/models/car_model.dart';
import 'package:aby_seyara/feature/presentations/users_screens/components/car_details_screen/car_details_screen.dart';
import 'package:flutter/material.dart';

import '../../../utils/app_theme.dart';

class CarCardInCompany extends StatelessWidget {
  const CarCardInCompany({
    Key? key,
    this.textColor = Colors.white,
    required this.car,
    this.companyImage,
  }) : super(key: key);
  final CarModel car;
  final Color? textColor;
  final String? companyImage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            GestureDetector(
              onTap: (() {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => CarDetailsScreen(
                      car: car,
                    ),
                  ),
                );
              }),
              child: Container(
                height: 350,
                width: 300,
                margin: const EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  image: DecorationImage(
                    image: NetworkImage(car.carImage![0]),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: Container(
                margin: const EdgeInsets.only(
                  bottom: 10,
                ), //10
                height: 100, //140
                width: 90,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Colors.white,
                    width: 1, //8
                  ),
                  image: DecorationImage(
                    fit: BoxFit.contain,
                    image: NetworkImage(companyImage!),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                car.title!,
                style: AppTheme.introHeadline.copyWith(color: textColor),
              ),
              Text(
                car.price!,
                style: AppTheme.whiteText.copyWith(color: textColor),
              )
            ],
          ),
        ),
      ],
    );
  }
}
