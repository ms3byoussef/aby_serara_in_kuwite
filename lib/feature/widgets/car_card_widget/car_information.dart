import 'package:flutter/material.dart';

import '../../../data/models/car_model.dart';
import '../../../utils/app_theme.dart';

class CarInformation extends StatelessWidget {
  const CarInformation({
    Key? key,
    @required this.car,
  }) : super(key: key);

  final CarModel? car;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(left: 24, right: 24, top: 50),
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: AppTheme.primaryColor),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('${car!.title}',
              overflow: TextOverflow.clip,
              style: AppTheme.introHeadline.copyWith(color: Colors.white)),
          Text('\$${car!.price}',
              style: AppTheme.introHeadline.copyWith(color: Colors.white)),
          Text(
            'KWD',
            style: AppTheme.whiteText,
          ),
          const SizedBox(
            height: 16,
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     Attribute(
          //       value: car!.carDetails!.brand,
          //       name: 'Brand',
          //     ),
          //     // Attribute(
          //     //   value: car.model,
          //     //   name: 'Model No',
          //     // ),
          //     // Attribute(
          //     //   value: car.co2,
          //     //   name: 'CO2',
          //     // ),
          //     Attribute(
          //       value: car!.carDetails!.fuel,
          //       name: 'Fuel Cons.',
          //     ),
          //   ],
          // )
        ],
      ),
    );
  }
}
