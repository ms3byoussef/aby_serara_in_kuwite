import 'package:aby_seyara/feature/presentations/users_screens/company_screens/company_view_model.dart';
import 'package:aby_seyara/trans/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../data/models/car_model.dart';
import '../../../redux/app/app_state.dart';
import '../../presentations/users_screens/components/car_details_screen/car_details_screen.dart';
import 'car_information.dart';

class CarCard extends StatelessWidget {
  final CarModel car;

  const CarCard({Key? key, required this.car}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CompanyViewModel>(
      builder: (_, viewModel) => _CarCard(
        viewModel: viewModel,
        car: car,
      ),
      converter: (store) {
        return CompanyViewModel.fromStore(store);
      },
    );
  }
}

class _CarCard extends StatelessWidget {
  final CompanyViewModel viewModel;
  const _CarCard({
    required this.car,
    required this.viewModel,
    Key? key,
  }) : super(key: key);

  final CarModel car;

  @override
  Widget build(BuildContext context) {
    // Car car = carList[index];
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return CarDetailsScreen(
                car: car,
              );
            },
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 16),
        child: Stack(
          children: [
            CarInformation(
              car: car,
            ),
            Translations.of(context)!.currentLanguage == 'ar'
                ? Positioned(
                    left: 35,
                    top: -30,
                    child: Hero(
                      tag: car.id,
                      child: car.id.isNotEmpty
                          ? FadeInImage.assetNetwork(
                              width: 120,
                              height: 120,
                              fit: BoxFit.contain,
                              placeholderCacheHeight: 10,
                              placeholderCacheWidth: 10,
                              placeholder: 'assets/images/loading.gif',
                              image: car.carImage!.first)
                          : Image.asset("assets/images/loading.gif"),
                    ),
                  )
                : Positioned(
                    right: 35,
                    top: -30,
                    child: Hero(
                      tag: car.id,
                      child: car.carImage!.isNotEmpty
                          ? FadeInImage.assetNetwork(
                              width: 120,
                              height: 120,
                              fit: BoxFit.contain,
                              placeholderCacheHeight: 10,
                              placeholderCacheWidth: 10,
                              placeholder: 'assets/images/loading.gif',
                              image: car.carImage![0])
                          : Image.asset("assets/images/loading.gif"),
                    ),
                  ),
            viewModel.user!.id == car.companyID
                ? Positioned(
                    right: 10,
                    top: -10,
                    child: IconButton(
                      icon: const Icon(Icons.delete_forever, color: Colors.red),
                      onPressed: () async {
                        showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            content: const Text("Do you need Delete Car ?"),
                            title: const Text(" Delete Car ?"),
                            actions: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  showTopSnackBar(
                                    context,
                                    const CustomSnackBar.success(
                                        message: "this Car didn't delete yet!"),
                                  );
                                },
                                child: const Text("No"),
                              ),
                              ElevatedButton(
                                  onPressed: () async {
                                    viewModel.removeCar!(car.id);

                                    showTopSnackBar(
                                      context,
                                      const CustomSnackBar.info(
                                        message: "this Request is deleted ",
                                      ),
                                    );
                                    Navigator.pop(context);
                                  },
                                  child: const Text("yes"))
                            ],
                            elevation: 24,
                          ),
                        );
                      },
                    ))
                : const SizedBox()
          ],
        ),
      ),
    );
  }
}
