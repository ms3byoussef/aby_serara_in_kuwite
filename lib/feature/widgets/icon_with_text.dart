import 'package:flutter/material.dart';

class RowWTextAndIcon extends StatelessWidget {
  final String? text;
  final Widget? icon;
  final TextStyle? style;
  const RowWTextAndIcon({
    Key? key,
    this.text,
    this.icon,
    this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        icon!,
        const SizedBox(
          width: 20,
        ),
        Text(
          text!,
          // ignore: prefer_if_null_operators
          style: style == null
              ? const TextStyle(
                  color: Colors.black,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'OpenSans',
                  fontSize: 20.0)
              : style,
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
