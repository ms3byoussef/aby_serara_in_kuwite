import 'package:aby_seyara/utils/app_theme.dart';
import 'package:flutter/material.dart';

class CarDetailsCard extends StatelessWidget {
  final String title;
  final Widget iconsOrText;
  const CarDetailsCard(
      {Key? key, required this.title, required this.iconsOrText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 110,
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(31),
        gradient: const LinearGradient(
          colors: [Color(0xff3c80f7), Color(0xff1058d1)],
          stops: [0, 1],
          begin: Alignment(-1.00, 0.00),
          end: Alignment(1.00, -0.00),
          // angle: 90,
          // scale: undefined,
        ),
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            iconsOrText,
            const SizedBox(
              height: 8,
            ),
            RichText(
              text: TextSpan(
                style: AppTheme.whiteText,
                children: <TextSpan>[
                  TextSpan(text: title),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
