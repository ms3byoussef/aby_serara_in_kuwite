import 'package:flutter/material.dart';

import '../../trans/translations.dart';
import '../../utils/app_theme.dart';
import '../presentations/users_screens/components/search_screen/search_screen.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: Colors.white,
      ),
      child: TextFormField(
        scrollPadding: const EdgeInsets.symmetric(horizontal: 5),
        onTap: () => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const SearchScreen())),
        keyboardType: TextInputType.text,
        style: AppTheme.whiteText
            .copyWith(color: AppTheme.primaryColor, fontSize: 16),
        decoration: InputDecoration(
          border: InputBorder.none,
          enabledBorder: InputBorder.none,
          hintText: Translations.of(context)!.text("search").toUpperCase(),
          // contentPadding:
          //     const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          hintStyle: AppTheme.hintText
              .copyWith(color: AppTheme.primaryColor, fontSize: 16),
          suffixIcon: IconButton(
            icon: Icon(Icons.search_rounded, color: AppTheme.primaryColor),
            onPressed: () {
              // Navigator.of(context).push(
              //     MaterialPageRoute(builder: (_) => const SearchScreen()));
            },
          ),
        ),
      ),
    );
  }
}
