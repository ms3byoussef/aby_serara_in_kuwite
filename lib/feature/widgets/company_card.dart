import 'package:flutter/material.dart';
import '../../data/models/user_model.dart';
import '../../utils/app_theme.dart';
import '../presentations/users_screens/components/company_details_screen/company_details_screen.dart';

class CompanyCard extends StatelessWidget {
  final UserModel company;
  const CompanyCard({Key? key, required this.company}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(right: 16, left: 0),
      width: 125,
      // height: 130,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: (() {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => CompanyDetailsScreen(company: company)));
            }),
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(15),
                ),
              ),
              height: 70,
              child: Hero(
                tag: company.id!,
                child: company.photo!.isNotEmpty
                    ? FadeInImage.assetNetwork(
                        height: 70,
                        fit: BoxFit.contain,
                        placeholderCacheHeight: 16,
                        placeholderCacheWidth: 16,
                        placeholder: 'assets/images/loading.gif',
                        image: company.photo!)
                    : Image.asset("assets/images/loading.gif"),
              ),
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          Text(
            company.name!,
            overflow: TextOverflow.ellipsis,
            style: AppTheme.introHeadline.copyWith(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              height: 1,
            ),
          ),
        ],
      ),
    );
  }
}
