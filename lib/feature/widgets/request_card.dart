import 'package:aby_seyara/feature/presentations/users_screens/client_screens/client_main_view.dart';
import 'package:aby_seyara/feature/presentations/users_screens/company_screens/company_main_view.dart';
import 'package:aby_seyara/feature/presentations/users_screens/components/request_details_screen/request_details_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../data/models/request_model.dart';
import '../../redux/action_report.dart';
import '../../redux/app/app_state.dart';
import '../../trans/translations.dart';
import '../../utils/app_theme.dart';
import '../../utils/progress_dialog.dart';
import '../presentations/users_screens/client_screens/client_view_model.dart';
import 'icon_with_text.dart';

class RequestCard extends StatelessWidget {
  final RequestModel? request;

  const RequestCard({
    this.request,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      builder: (_, viewModel) => _RequestCard(
        viewModel: viewModel,
        request: request,
      ),
      converter: (store) {
        return ClientViewModel.fromStore(store);
      },
    );
  }
}

class _RequestCard extends StatefulWidget {
  final ClientViewModel? viewModel;
  final RequestModel? request;
  const _RequestCard({Key? key, this.request, this.viewModel})
      : super(key: key);

  @override
  __RequestCardState createState() => __RequestCardState();
}

class __RequestCardState extends State<_RequestCard> {
  @override
  void initState() {
    widget.viewModel!.getRequestUser!(widget.request!);
    widget.viewModel!.getCarOwnerUser!(widget.request!.car!);

    super.initState();
  }

  ProgressDialog? progressDialog;

  @override
  void didUpdateWidget(_RequestCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel!.removeRequestReport?.status ==
          ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (progressDialog!.isShowing()) {
          progressDialog!.setMessage("Cancel Order ...");
          progressDialog!.show();
        }
      } else {
        if (widget.viewModel!.removeRequestReport?.status ==
            ActionStatus.error) {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
          showTopSnackBar(
            context,
            CustomSnackBar.error(
              message: widget.viewModel!.removeRequestReport!.msg.toString(),
            ),
          );
        } else if (widget.viewModel!.removeRequestReport?.status ==
            ActionStatus.complete) {
          if (widget.viewModel!.currentUser!.role == "client") {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (_) => const ClientMainView()),
                (route) => false);
          } else if (widget.viewModel!.currentUser!.role == "company") {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (_) => const CompanyMainView()),
                (route) => false);
          }
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }

          widget.viewModel!.removeRequestReport?.status = null;
        } else {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
        }
      }
      widget.viewModel!.removeRequestReport?.status = null;
    });
  }

  deleteRequest() {
    widget.viewModel!.removeRequest!(widget.request!);
    showTopSnackBar(
      context,
      CustomSnackBar.error(
        message: "Delate the Request",
        textStyle: AppTheme.introHeadline.copyWith(color: Colors.white),
      ),
    );
  }

  updateRequestStatus() {
    widget.viewModel!.updateRequest!(widget.request!.id, "accepted");
  }

  @override
  Widget build(BuildContext context) {
    return widget.request!.car != null
        ? GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) =>
                      RequestDetailsScreen(request: widget.request!)));
            },
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              padding: const EdgeInsets.symmetric(horizontal: 12),
              height: 140,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(14)),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x29000000),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
                color: Color(0xffffffff),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                      onTap: () {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (_) => OrderDetails(
                        //               order: widget.order,
                        //             )));
                      },
                      child: Image.network(
                        widget.request!.car!.carImage![0].toString(),
                        width: 90,
                        fit: BoxFit.contain,
                      )),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 6),
                          child: Text(widget.request!.type.toString(),
                              maxLines: 2, style: AppTheme.cardText)),
                      Text.rich(
                        TextSpan(children: [
                          TextSpan(
                            text: widget.request!.car!.price.toString(),
                          ),
                          TextSpan(text: Translations.of(context)!.text("kwd")),
                        ], style: AppTheme.hintText),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 6),
                        child: widget.viewModel!.requestUser != null &&
                                widget.viewModel!.currentUser!.role != "client"
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RowWTextAndIcon(
                                    text: widget.viewModel!.requestUser!.name,
                                    style: AppTheme.cardText,
                                    icon: const Icon(Icons.person),
                                  ),
                                  RowWTextAndIcon(
                                    text: widget.viewModel!.requestUser!.phone,
                                    style: AppTheme.cardText,
                                    icon: const Icon(Icons.phone),
                                  ),
                                ],
                              )
                            : Container(),
                      ),
                    ],
                  ),
                  Opacity(
                    opacity: .7,
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            content: Text(Translations.of(context)!
                                .text("is_request_accepted")),
                            title: const Text(" done?"),
                            actions: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  showTopSnackBar(
                                    context,
                                    CustomSnackBar.info(
                                        message: Translations.of(context)!
                                            .text("request_not_accepted")),
                                  );
                                },
                                child:
                                    Text(Translations.of(context)!.text("no")),
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    updateRequestStatus();
                                    Navigator.pop(context);

                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.success(
                                          message: Translations.of(context)!
                                              .text("accepted_request")),
                                    );
                                  },
                                  child: Text(
                                      Translations.of(context)!.text("yes")))
                            ],
                            elevation: 24,
                          ),
                        );
                      },
                      child: widget.viewModel!.currentUser!.role == "company"
                          ? Row(
                              children: [
                                (widget.request!.status == 'accepted')
                                    ? Image.asset(
                                        "assets/images/approved.png",
                                        height: 80,
                                      )
                                    : SvgPicture.asset(
                                        "assets/icons/check.svg",
                                        height: 35,
                                      ),
                                const SizedBox(
                                  width: 4,
                                ),
                              ],
                            )
                          : Column(
                              children: [
                                (widget.request!.status == 'accepted')
                                    ? Image.asset(
                                        "assets/images/approved.png",
                                        height: 80,
                                      )
                                    : widget.request!.status == 'rejected'
                                        ? Image.asset(
                                            "assets/images/rejected.png",
                                            height: 80,
                                            fit: BoxFit.cover,
                                          )
                                        : const SizedBox()
                              ],
                            ),
                    ),
                  ),
                  Opacity(
                    opacity: .7,
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            content: Text(
                                Translations.of(context)!.text("do_cancel")),
                            title:
                                Text(Translations.of(context)!.text("cancel")),
                            actions: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  showTopSnackBar(
                                    context,
                                    CustomSnackBar.success(
                                        message: Translations.of(context)!
                                            .text("request_not_canceled")),
                                  );
                                },
                                child:
                                    Text(Translations.of(context)!.text("no")),
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    widget.viewModel!
                                        .removeRequest!(widget.request!);

                                    showTopSnackBar(
                                      context,
                                      const CustomSnackBar.info(
                                        message: "your Request is Canceled",
                                      ),
                                    );
                                  },
                                  child: Text(
                                      Translations.of(context)!.text("yes")))
                            ],
                            elevation: 24,
                          ),
                        );
                      },
                      child: SvgPicture.asset(
                        "assets/icons/cancel.svg",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        : Container();
  }
}
