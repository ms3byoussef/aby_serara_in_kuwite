import 'default_button.dart';
import 'package:flutter/material.dart';
import '../../data/models/user_model.dart';
import '../../utils/app_theme.dart';
import '../presentations/users_screens/components/company_details_screen/company_details_screen.dart';
import 'icon_with_text.dart';

class BigCompanyCard extends StatelessWidget {
  final UserModel company;
  const BigCompanyCard({Key? key, required this.company}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 22),
      width: double.infinity,
      height: 260,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
              color: Color(0xd5d3d3d3),
              offset: Offset(0, 10),
              blurRadius: 33,
              spreadRadius: 0)
        ],
        color: Color(0xffffffff),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              InkWell(
                onTap: (() => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => CompanyDetailsScreen(
                              company: company,
                            )))),
                child: Hero(
                  tag: company.id!,
                  child: company.photo!.isNotEmpty
                      ? FadeInImage.assetNetwork(
                          height: 70,
                          fit: BoxFit.contain,
                          placeholderCacheHeight: 16,
                          placeholderCacheWidth: 16,
                          placeholder: 'assets/images/loading.gif',
                          image: company.photo!)
                      : Image.asset("assets/images/loading.gif"),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Text(
              company.name!,
              style: AppTheme.introHeadline,
            ),
          ),
          RowWTextAndIcon(
            icon: const Icon(Icons.email),
            text: company.email!,
          ),
          RowWTextAndIcon(
            icon: const Icon(Icons.phone),
            text: company.phone!,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [],
            ),
          ),
          DefaultButton(
            height: 40,
            press: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (_) => CompanyDetailsScreen(
                      company: company,
                    ),
                  ),
                  (route) => false);
            },
            text: 'Show Now',
          ),
        ],
      ),
    );
  }
}
