import 'car_model.dart';

class RequestModel {
  String id;
  String? userID;
  CarModel? car;
  String? orderTime;
  String? agreementFile;
  String? status;
  String? type;

  RequestModel({
    this.id = "",
    this.userID,
    this.car,
    this.orderTime,
    this.status,
    this.agreementFile,
    this.type,
  });

  RequestModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'],
        userID = map['userID'],
        car = CarModel.fromJson(map['car']),
        orderTime = map['orderTime'],
        status = map['status'],
        agreementFile = map['agreementFile'],
        type = map['type'];

  Map<dynamic, dynamic> toJson() => {
        'id': id,
        'userID': userID,
        'car': car!.toJson(),
        'orderTime': orderTime,
        'status': status,
        'agreementFile': agreementFile,
        'type': type,
      };

  RequestModel copyWith({
    String id = "",
    String? userID,
    CarModel? car,
    String? orderTime,
    String? status,
    String? agreementFile,
    String? type,
  }) {
    return RequestModel(
      id: id,
      userID: userID,
      car: car,
      orderTime: orderTime,
      status: status,
      agreementFile: agreementFile,
      type: type,
    );
  }
}
