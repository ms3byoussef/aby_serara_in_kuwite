class CarDetailsModel {
  final String? brand;
  final String? modelYear;
  final String? modelBody;
  final String? fuel;
  final String? engine;
  final String? transmission;
  final String? numberOfAirbag;
  final String? numberOfGears;
  final String? numberOfSeating;
  final List<int>? colors;
  CarDetailsModel({
    this.brand,
    this.modelYear,
    this.modelBody,
    this.fuel,
    this.engine,
    this.transmission,
    this.numberOfAirbag,
    this.numberOfGears,
    this.numberOfSeating,
    this.colors,
  });
  CarDetailsModel.fromJson(Map<dynamic, dynamic> map)
      : brand = map['brand'],
        modelYear = map['modelYear'],
        modelBody = map['modelBody'],
        fuel = map['fuel'],
        engine = map['engine'],
        transmission = map['transmission'],
        colors =
            map['colors'] == null ? [] : List.of(map['colors'].cast<int>()),
        numberOfAirbag = map['numberOfAirbag'],
        numberOfGears = map['numberOfGears'],
        numberOfSeating = map['numberOfSeating'];

  Map<String, Object?> toJson() => {
        'brand': brand ?? "",
        'modelYear': modelYear ?? "",
        'modelBody': modelBody ?? "",
        'fuel': fuel ?? "",
        'engine': engine ?? "",
        'transmission': transmission ?? "",
        'numberOfAirbag': numberOfAirbag ?? "",
        'numberOfGears': numberOfGears ?? "",
        'numberOfSeating': numberOfSeating ?? "",
        if (colors != null)
          'colors': colors?.map((e) => e.toInt()).toList() ?? [],
      };

  CarDetailsModel copyWith({
    final String? brand,
    final String? modelYear,
    final String? modelBody,
    final String? fuel,
    final String? engine,
    final String? transmission,
    final String? numberOfAirbag,
    final String? numberOfGears,
    final String? numberOfSeating,
    final List<int>? colors,
  }) {
    return CarDetailsModel(
      brand: brand ?? this.brand,
      modelYear: modelYear ?? this.modelYear,
      modelBody: modelBody ?? this.modelBody,
      fuel: fuel ?? this.fuel,
      engine: engine ?? this.engine,
      transmission: transmission ?? this.transmission,
      numberOfAirbag: numberOfAirbag ?? this.numberOfAirbag,
      numberOfGears: numberOfGears ?? this.numberOfGears,
      numberOfSeating: numberOfSeating ?? this.numberOfSeating,
      colors: colors ?? this.colors,
    );
  }
}
