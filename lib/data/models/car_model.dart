import 'car_details_model.dart';

class CarModel {
  String id;
  String? companyID;
  String? title;
  String? description;
  String? price;
  List<String>? carImage;
  CarDetailsModel? carDetails;

  CarModel({
    this.id = "",
    this.companyID,
    this.title,
    this.description,
    this.price,
    this.carImage,
    this.carDetails,
  });

  CarModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'],
        title = map['title'],
        price = map['price'],
        companyID = map['companyID'],
        description = map['description'],
        carImage = map['carImage'].cast<String>(),
        carDetails = CarDetailsModel.fromJson(map['carDetails']);

  Map<String, Object?> toJson() => {
        'id': id,
        'title': title,
        'price': price,
        'companyID': companyID,
        'description': description,
        'carImage': carImage?.map((e) => e.toString()).toList() ?? [],
        'carDetails': carDetails?.toJson(),
      };

  CarModel copyWith({
    String id = "",
    String? title,
    String? price,
    String? companyID,
    String? description,
    List<String>? carImage,
    CarDetailsModel? carDetails,
  }) {
    return CarModel(
      id: id,
      title: title,
      price: price,
      companyID: companyID,
      description: description,
      carImage: carImage,
      carDetails: carDetails,
    );
  }
}
