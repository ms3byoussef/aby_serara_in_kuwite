class TermsModel {
  String? id;
  String? termsContentEn;
  String? termsContentAr;
  String? type;

  TermsModel({
    this.id,
    this.termsContentEn,
    this.termsContentAr,
    this.type,
  });

  TermsModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'],
        termsContentEn = map['termsContentEn'],
        termsContentAr = map['termsContentAr'],
        type = map['type'];

  Map<String, Object?> toJson() => {
        'id': id,
        'termsContentEn': termsContentEn,
        'termsContentAr': termsContentAr,
        'type': type,
      };

  TermsModel copyWith({
    String id = "",
    String? termsContentEn,
    String? termsContentAr,
    String? type,
  }) {
    return TermsModel(
      id: id,
      termsContentEn: termsContentEn,
      termsContentAr: termsContentAr,
      type: type,
    );
  }
}
