class UserModel {
  String? id;
  String? name;
  String? phone;
  String? email;
  String? address;
  String? photo;
  String role;

  UserModel(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.address,
      this.photo,
      this.role = "client"});

  UserModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'] ?? "",
        name = map['name'] ?? "",
        email = map['email'] ?? "",
        phone = map['phone'] ?? "",
        address = map['address'] ?? "",
        photo = map["photo"] ?? "",
        role = map['role'] ?? "";

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
        'phone': phone,
        'address': address,
        'photo': photo,
        'role': role,
      };

  UserModel copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? address,
    String? photo,
    String? role,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      address: address ?? this.address,
      photo: photo ?? this.photo,
      role: role ?? this.role,
    );
  }
}
