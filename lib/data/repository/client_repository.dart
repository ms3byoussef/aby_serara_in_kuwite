import 'package:aby_seyara/data/models/terms_model.dart';
import 'package:firebase_database/firebase_database.dart';

import '../models/car_model.dart';
import '../models/request_model.dart';
import '../models/user_model.dart';

class ClientRepository {
  const ClientRepository();

  Future<RequestModel> postRequest(
    RequestModel request,
  ) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('requests').push();
    request.id = databaseReference.key!;

    databaseReference.set(request.toJson());
    var requestSnapshot = await databaseReference.once();

    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    RequestModel requestModel = RequestModel.fromJson(map);
    requestModel.id = requestSnapshot.snapshot.key!;

    return requestModel;
  }

  Future<List<RequestModel>> getRequests() async {
    List<RequestModel> requests = [];
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("requests");
    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    map.forEach((key, value) async {
      value["id"] = key;
      RequestModel request = RequestModel.fromJson(value);

      requests.add(request);
    });

    return requests;
  }

  Future<UserModel> getRequestUser(RequestModel request) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('users/${request.userID}');

    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    UserModel requestUser = UserModel.fromJson(map);
    requestUser.id = requestSnapshot.snapshot.key!;
    return requestUser;
  }

  Future<UserModel> getCarOwnerUser(CarModel carModel) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('users/${carModel.companyID}');

    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    UserModel company = UserModel.fromJson(map);
    company.id = requestSnapshot.snapshot.key!;
    return company;
  }

  // Future<CarModel> getCarInRequest(RequestModel request) async {
  //   DatabaseReference databaseReference =
  //       FirebaseDatabase.instance.ref().child('cars/${request.car!.id}');

  //   var requestSnapshot = await databaseReference.once();
  //   Map<dynamic, dynamic> map =
  //       requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
  //   CarModel carInRequest = CarModel.fromJson(map);
  //   carInRequest.id = requestSnapshot.snapshot.key!;

  //   return carInRequest;
  // }

  Future<RequestModel> updateRequest(String requestId, String status) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("requests/$requestId");

    databaseReference.update({"status": status});
    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    RequestModel requestModel = RequestModel.fromJson(map);
    requestModel.id = requestSnapshot.snapshot.key!;

    return requestModel;
  }

  removeRequest(RequestModel request) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("requests/${request.id}");

    databaseReference.remove();
  }

  Future<TermsModel> getTerms(String type) async {
    // TermsModel termsModel;

    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('terms');

    var termsSnapshot =
        await databaseReference.orderByChild('type').equalTo(type).once();
    Map<dynamic, dynamic> map =
        termsSnapshot.snapshot.value as Map<dynamic, dynamic>;
    TermsModel terms = TermsModel.fromJson(map.values.first);
    terms.id = termsSnapshot.snapshot.key!;
    return terms;
  }

  Future<List<UserModel>> getClients() async {
    // print("getEmployees");
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("users");
    var serviceSnapshot = await databaseReference.once();
    List<UserModel> clients = [];

    Map<dynamic, dynamic> map =
        serviceSnapshot.snapshot.value as Map<dynamic, dynamic>;
    map.forEach((key, value) async {
      if (value['role'] == "client") {
        UserModel client = UserModel.fromJson(value);
        client.id = key;
        clients.add(client);
        // print(map);
      }
    });

    return clients;
  }
}
