// ignore_for_file: no_leading_underscores_for_local_identifiers, unnecessary_null_comparison

import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user_model.dart';

class AuthRepository {
  const AuthRepository();
  Future<UserModel?> login(String email, String password) async {
    UserModel? user;

    final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    await firebaseAuth
        .signInWithEmailAndPassword(email: email.trim(), password: password)
        .then((value) async {
      final usersReferences = FirebaseDatabase.instance.ref().child('users');

      var userSnapshot = await usersReferences.child(value.user!.uid).once();

      Map<dynamic, dynamic> map =
          userSnapshot.snapshot.value as Map<dynamic, dynamic>;
      user = UserModel.fromJson(map);
      user!.id = value.user!.uid;
      sharedPreferences.setBool("isLogin", true);
      const JsonEncoder _encoder = JsonEncoder();

      sharedPreferences.setString("users", _encoder.convert((user!.toJson())));
    });

    return user;
  }

  Future<UserModel?> signUp({UserModel? user, String? password}) async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var usersReferences = FirebaseDatabase.instance.ref().child("users");
    await firebaseAuth
        .createUserWithEmailAndPassword(
            email: user!.email!.trim(), password: password!.trim())
        .then((value) async {
      {
        user!.id = value.user!.uid;
        await usersReferences.child(value.user!.uid).set(user!.toJson());
        var userSnapshot = await usersReferences.child(value.user!.uid).once();
        Map<dynamic, dynamic> map =
            userSnapshot.snapshot.value as Map<dynamic, dynamic>;
        user = UserModel.fromJson(map);
        user!.id = value.user!.uid;
        sharedPreferences.setBool("isLogin", true);
        const JsonEncoder _encoder = JsonEncoder();

        sharedPreferences.setString(
            "users", _encoder.convert((user!.toJson())));
      }
    });
    return user;
  }

  Future<UserModel?> updateProfile(UserModel userModel, {File? img}) async {
    UserModel user = UserModel();
    if (img != null) {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      firebase_storage.Reference ref =
          storage.ref().child("user_images").child(img.toString());
      final uploadTask = ref.putFile(img);

      // ignore: avoid_print
      await uploadTask.whenComplete(() => print("complategetDownloadURL"));
      user.photo = await ref.getDownloadURL();
    } else {}
    var userAuth = (FirebaseAuth.instance.currentUser);
    user.id = userAuth!.uid;
    user.photo = userAuth.photoURL;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var usersReferences =
        FirebaseDatabase.instance.ref().child("users/${userAuth.uid}");

    await usersReferences.update(userModel.toJson());

    var userSnapshot = await usersReferences.once();
    Map<dynamic, dynamic> map =
        userSnapshot.snapshot.value as Map<dynamic, dynamic>;
    user = UserModel.fromJson(map);
    user.id = userAuth.uid;
    const JsonEncoder _encoder = JsonEncoder();

    sharedPreferences.setString("users", _encoder.convert((user.toJson())));

    return user;
  }

  Future<String> getPlace(LatLng latLong) async {
    List<Placemark> newPlace =
        await placemarkFromCoordinates(latLong.latitude, latLong.longitude);

    // this is all you need
    Placemark placeMark = newPlace[0];
    String? name = placeMark.name;
    String? subLocality = placeMark.subLocality;
    String? locality = placeMark.locality;
    String? administrativeArea = placeMark.administrativeArea;
    String? postalCode = placeMark.postalCode;
    String? country = placeMark.country;
    String address =
        "$name, $subLocality, $locality, $administrativeArea $postalCode, $country";
    return address;
  }

  Future<UserModel> addCompany(
      UserModel company, String password, File img) async {
    if (img == null) {
      company.photo = "";
    } else {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      firebase_storage.Reference ref =
          storage.ref().child("company_images").child(img.toString());
      final uploadTask = ref.putFile(img);

      // ignore: avoid_print
      await uploadTask.whenComplete(() => print("complategetDownloadURL"));
      company.photo = await ref.getDownloadURL();
    }

    FirebaseAuth firebaseAuth = FirebaseAuth.instance;

    await firebaseAuth
        .createUserWithEmailAndPassword(
            email: company.email!.trim(), password: password.trim())
        .then((value) async {
      {
        company.id = value.user!.uid;

        var usersReferences = FirebaseDatabase.instance.ref().child("users");

        await usersReferences.child(value.user!.uid).set(company.toJson());
        var userSnapshot = await usersReferences.child(value.user!.uid).once();
        Map<dynamic, dynamic> map =
            userSnapshot.snapshot.value as Map<dynamic, dynamic>;
        company = UserModel.fromJson(map);
      }
    });
    return company;
  }

  Future<List<UserModel>> getCompanies() async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("users");
    var serviceSnapshot = await databaseReference.once();
    List<UserModel> companies = [];

    Map<dynamic, dynamic> map =
        serviceSnapshot.snapshot.value as Map<dynamic, dynamic>;
    map.forEach((key, value) async {
      if (value['role'] == "company") {
        UserModel company = UserModel.fromJson(value);
        company.id = key;
        companies.add(company);
        // print(map);
      }
    });

    return companies;
  }

  removeCompany(String companyId) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("users/$companyId");

    databaseReference.remove().whenComplete(() {});
  }
}
