import 'package:firebase_database/firebase_database.dart';

import '../models/car_model.dart';
import '../models/terms_model.dart';

class CompanyRepository {
  const CompanyRepository();

  Future<CarModel> postCar(CarModel? carModel) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('cars').push();

    carModel!.id = databaseReference.key!;

    databaseReference.set(carModel.toJson());
    var carSnapshot = await databaseReference.once();

    Map<dynamic, dynamic> map =
        carSnapshot.snapshot.value as Map<dynamic, dynamic>;
    CarModel car = CarModel.fromJson(map);
    carModel.id = carSnapshot.snapshot.key!;

    return car;
  }

  Future<List<CarModel>> getCars() async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars");
    var carSnapshot = await databaseReference.once();
    List<CarModel> cars = [];

    Map<dynamic, dynamic> map =
        carSnapshot.snapshot.value as Map<dynamic, dynamic>;

    map.forEach((key, value) async {
      value["id"] = key;
      CarModel car = CarModel.fromJson(value);

      cars.add(car);
    });

    return cars;
  }

  Future<CarModel> editCar(
    CarModel? carModel,
  ) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars/${carModel!.id}");
    databaseReference.update(
      carModel.toJson(),
    );
    var carSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        carSnapshot.snapshot.value as Map<dynamic, dynamic>;
    CarModel car = CarModel.fromJson(map);
    car.id = carSnapshot.snapshot.key!;

    return car;
  }

  removeCar(
    String carId,
  ) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars/$carId");

    databaseReference.remove();
  }

  Future<TermsModel> postTerms(TermsModel? termsModel) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('terms').push();

    termsModel!.id = databaseReference.key!;

    databaseReference.set(termsModel.toJson());
    var termsSnapshot = await databaseReference.once();

    Map<dynamic, dynamic> map =
        termsSnapshot.snapshot.value as Map<dynamic, dynamic>;
    TermsModel terms = TermsModel.fromJson(map);
    termsModel.id = termsSnapshot.snapshot.key!;

    return terms;
  }

  Future<TermsModel> editTerms(
    TermsModel? termsModel,
  ) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("terms/${termsModel!.id}");
    databaseReference.update(
      termsModel.toJson(),
    );
    var termsSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        termsSnapshot.snapshot.value as Map<dynamic, dynamic>;
    TermsModel term = TermsModel.fromJson(map);
    term.id = termsSnapshot.snapshot.key!;

    return term;
  }

  removeTerms(
    String termsID,
  ) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("terms/$termsID");

    databaseReference.remove();
  }
}
