// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:flutter/widgets.dart';

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

int convertToInt(Color color) {
  return color.value;
}

Color convertToColor(int intColor) {
  return Color(intColor);
}

void convertListToInt(List<Color> colors) {
  return colors.forEach((element) {
    element.value;
  });
}

convertListToColor(List<int> intColor) {
  return intColor.forEach((element) {
    Color(element);
  });
}
