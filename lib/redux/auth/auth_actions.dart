import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../data/models/user_model.dart';
import '../action_report.dart';

class AuthStatusAction {
  final String actionName = "AuthStatusAction";
  final ActionReport report;
  AuthStatusAction({required this.report});
}

class LoginAction {
  final String actionName = "LoginAction";
  final String? email;
  final String? password;

  LoginAction({required this.email, required this.password});
}

class SyncUserAction {
  final String actionName = "SyncUserAction";
  final UserModel? user;
  SyncUserAction(this.user);
}

class SignUpAction {
  final String actionName = "SignUpAction";
  UserModel? user;
  String? password;
  SignUpAction({this.user, this.password});
}

class UpdateProfileAction {
  final String actionName = "UpdateProfileAction";
  final UserModel user;
  final File? img;

  UpdateProfileAction({required this.user, this.img});
}

class AddressAction {
  final String actionName = "AddressAction";
  final LatLng latLong;

  const AddressAction({
    required this.latLong,
  });
}

class SyncAddressAction {
  final String actionName = "SyncAddressAction";

  final String address;
  SyncAddressAction(this.address);
}

class AddCompanyAction {
  final String actionName = "AddCompanyAction";
  final UserModel? company;
  final String? password;
  final File? img;

  AddCompanyAction({this.company, this.password, this.img});
}

class SyncCompanyAction {
  final String actionName = "SyncCompanyAction";

  final UserModel? company;
  SyncCompanyAction({this.company});
}

class SyncCompaniesAction {
  final String actionName = "SyncCompaniesAction";

  List<UserModel>? companies;
  SyncCompaniesAction({this.companies});
}

class GetCompaniesAction {
  final String actionName = "GetCompaniesAction";

  GetCompaniesAction();
}

class RemoveCompanyAction {
  final String actionName = "RemoveCompanyAction";

  final String? companyId;
  RemoveCompanyAction({
    this.companyId,
  });
}
