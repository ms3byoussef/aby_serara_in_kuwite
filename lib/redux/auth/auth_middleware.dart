// ignore_for_file: avoid_print, no_leading_underscores_for_local_identifiers

import 'package:redux/redux.dart';
import '../../data/repository/auth_repository.dart';
import '../action_report.dart';
import '../app/app_state.dart';
import 'auth_actions.dart';

List<Middleware<AppState>> createAuthMiddleware([
  AuthRepository _repository = const AuthRepository(),
]) {
  final login = _login(_repository);
  final signUp = _signUp(_repository);
  final updateProfile = _updateProfile(_repository);
  final address = _address(_repository);
  final getCompanies = _getCompanies(_repository);
  final addCompany = _addCompany(_repository);
  final removeCompany = _removeCompany(_repository);
  return [
    TypedMiddleware<AppState, LoginAction>(login),
    TypedMiddleware<AppState, SignUpAction>(signUp),
    TypedMiddleware<AppState, UpdateProfileAction>(updateProfile),
    TypedMiddleware<AppState, AddressAction>(address),
    TypedMiddleware<AppState, GetCompaniesAction>(getCompanies),
    TypedMiddleware<AppState, AddCompanyAction>(addCompany),
    TypedMiddleware<AppState, RemoveCompanyAction>(removeCompany),
  ];
}

Middleware<AppState> _login(AuthRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.login(action.email!, action.password!).then((user) {
      next(SyncUserAction(user));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _signUp(AuthRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .signUp(user: action.user, password: action.password)
        .then((user) {
      next(SyncUserAction(user));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _updateProfile(AuthRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .updateProfile(
      action.user,
      img: action.img,
    )
        .then((user) {
      next(SyncUserAction(user));
      completed(next, action);
    });

    // .catchError((error) {
    //   print(error);
    //   catchError(next, action, error);
    // });
  };
}

Middleware<AppState> _address(AuthRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getPlace(action.latLong).then((address) {
      next(SyncAddressAction(address));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _addCompany(AuthRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    // print("Middleware running");
    repository
        .addCompany(
      action.company,
      action.password,
      action.img,
    )
        .then((company) {
      next(SyncCompanyAction(company: company));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getCompanies(AuthRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    store.state.authState!.companies!.clear();

    repository.getCompanies().then((companies) {
      next(SyncCompaniesAction(companies: companies));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _removeCompany(AuthRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .removeCompany(
      action.company,
    )
        .then((company) {
      // next(SyncServiceAction(service: service));
      next(action);
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

void catchError(NextDispatcher next, action, error) {
  next(AuthStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: error.toString())));
}

void completed(NextDispatcher next, action) {
  next(AuthStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.complete,
          msg: "${action.actionName} is completed")));
}

void running(NextDispatcher next, action) {
  next(AuthStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.running,
          msg: "${action.actionName} is running")));
}

// ignore: unused_element
void idEmpty(NextDispatcher next, action) {
  next(AuthStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: "Id is empty")));
}
