import '../../data/models/user_model.dart';
import '../action_report.dart';

class AuthState {
  UserModel? user;
  Map<String, ActionReport>? status;
  String? address;
  UserModel? company;
  Map<String, UserModel>? companies;

  AuthState(
      {required this.status,
      required this.user,
      this.address,
      this.company,
      this.companies});

  factory AuthState.initial() {
    return AuthState(
      user: null,
      status: {},
      address: "",
      company: null,
      companies: {},
    );
  }

  AuthState copyWith({
    UserModel? user,
    Map<String, ActionReport>? status,
    UserModel? company,
    Map<String, UserModel>? companies,
    String? address,
  }) {
    return AuthState(
      user: user ?? this.user,
      status: status ?? this.status ?? {},
      address: address ?? this.address,
      company: company ?? this.company,
      companies: companies ?? this.companies,
    );
  }
}
