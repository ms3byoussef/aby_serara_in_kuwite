// ignore_for_file: avoid_print

import 'package:redux/redux.dart';
import 'auth_actions.dart';
import 'auth_state.dart';

final authReducer = combineReducers<AuthState>([
  TypedReducer<AuthState, AuthStatusAction>(_syncAuthState),
  TypedReducer<AuthState, SyncUserAction>(_syncUser),
  TypedReducer<AuthState, SyncAddressAction>(_syncAddress),
  TypedReducer<AuthState, SyncCompanyAction>(_syncCompany),
  TypedReducer<AuthState, SyncCompaniesAction>(_syncCompanies),
  TypedReducer<AuthState, RemoveCompanyAction>(_syncRemoveCompany),
]);

AuthState _syncAuthState(AuthState state, AuthStatusAction action) {
  var status = state.status ?? {};
  print(status);
  status.update(action.report.actionName!, (v) => action.report,
      ifAbsent: () => action.report);

  return state.copyWith(status: status);
}

AuthState _syncCompany(AuthState state, SyncCompanyAction action) {
  state.companies!.update(
      action.company!.id.toString(), (company) => action.company ?? company,
      ifAbsent: () => action.company!);

  return state.copyWith(company: action.company);
}

AuthState _syncCompanies(AuthState state, SyncCompaniesAction action) {
  for (var company in action.companies ?? []) {
    state.companies!.update(company.id.toString(), (company) => company,
        ifAbsent: () => company);
  }

  return state.copyWith(companies: state.companies);
}

AuthState _syncRemoveCompany(AuthState state, RemoveCompanyAction action) {
  state.companies?.remove(action.companyId);
  return state.copyWith(companies: state.companies);
}

AuthState _syncUser(AuthState state, SyncUserAction action) {
  state.user = action.user;
  return state.copyWith(user: action.user);
}

AuthState _syncAddress(AuthState state, SyncAddressAction action) {
  state.address = action.address;
  return state.copyWith(address: action.address);
}
