// ignore_for_file: avoid_print, no_leading_underscores_for_local_identifiers

import 'package:redux/redux.dart';
import '../../data/repository/company_repository.dart';
import '../action_report.dart';
import '../app/app_state.dart';
import 'company_actions.dart';

List<Middleware<AppState>> createCompanyMiddleware([
  CompanyRepository _repository = const CompanyRepository(),
]) {
  final postCar = _postCar(_repository);
  final getCars = _getCars(_repository);
  final editCar = _editCar(_repository);
  final removeCar = _removeCar(_repository);
  final postTerms = _postTerms(_repository);

  return [
    TypedMiddleware<AppState, PostCarAction>(postCar),
    TypedMiddleware<AppState, GetCarsAction>(getCars),
    TypedMiddleware<AppState, EditCarAction>(editCar),
    TypedMiddleware<AppState, RemoveCarAction>(removeCar),
    TypedMiddleware<AppState, PostTermsAction>(postTerms),
  ];
}

Middleware<AppState> _postCar(CompanyRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .postCar(
      action.car,
    )
        .then((car) {
      next(SyncCarAction(car: car));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getCars(CompanyRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    store.state.companyState!.cars.clear();
    repository.getCars().then((cars) {
      next(SyncCarsAction(cars: cars));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _editCar(CompanyRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .editCar(
      action.car,
    )
        .then((car) {
      next(SyncCarAction(car: car));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _removeCar(CompanyRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .removeCar(
      action.carId,
    )
        .then((car) {
      next(action);
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _postTerms(CompanyRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .postTerms(
      action.terms,
    )
        .then((terms) {
      next(SyncTermsAction(termsModel: terms));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

void catchError(NextDispatcher next, action, error) {
  next(CompanyStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: error.toString())));
}

void completed(NextDispatcher next, action) {
  next(CompanyStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.complete,
          msg: "${action.actionName} is completed")));
}

void running(NextDispatcher next, action) {
  next(CompanyStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.running,
          msg: "${action.actionName} is running")));
}

void idEmpty(NextDispatcher next, action) {
  next(CompanyStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: "Id is empty")));
}
