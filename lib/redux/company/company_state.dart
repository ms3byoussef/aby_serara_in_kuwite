import '../../data/models/terms_model.dart';

import '../../data/models/car_model.dart';
import '../../data/models/user_model.dart';
import '../action_report.dart';

class CompanyState {
  Map<String, ActionReport> status;
  CarModel? car;
  Map<String, CarModel> cars;
  UserModel? user;
  TermsModel? terms;
  Map<String, TermsModel>? allTerms;

  CompanyState({
    required this.status,
    required this.car,
    required this.cars,
    required this.user,
    this.terms,
    this.allTerms,
  });

  factory CompanyState.initial() {
    return CompanyState(
      car: null,
      user: null,
      status: {},
      cars: {},
      terms: null,
      allTerms: {},
    );
  }

  CompanyState copyWith({
    UserModel? user,
    CarModel? car,
    Map<String, ActionReport>? status,
    Map<String, CarModel>? cars,
    List<CarModel>? searchCar,
    TermsModel? terms,
    Map<String, TermsModel>? allTerms,
  }) {
    return CompanyState(
      car: car ?? this.car,
      user: user ?? this.user,
      status: status ?? this.status,
      cars: cars ?? this.cars,
      terms: terms ?? this.terms,
      allTerms: allTerms ?? this.allTerms,
    );
  }
}
