import '../../data/models/car_model.dart';
import '../../data/models/terms_model.dart';
import '../action_report.dart';

class CompanyStatusAction {
  final String actionName = "CompanyStatusAction";
  final ActionReport report;
  CompanyStatusAction({required this.report});
}

class PostCarAction {
  final String actionName = "PostCarAction";
  final CarModel? car;

  PostCarAction({
    this.car,
  });
}

class SyncCarAction {
  final String actionName = "SyncCarAction";
  final CarModel? car;

  SyncCarAction({this.car});
}

class SyncCarsAction {
  final String actionName = "SyncCarsAction";
  final List<CarModel>? cars;

  SyncCarsAction({this.cars});
}

class GetCarsAction {
  final String actionName = "GetCarsAction";

  GetCarsAction();
}

class GetCarAction {
  final String actionName = "GetCarAction";

  final String? carId;

  GetCarAction({this.carId});
}

class EditCarAction {
  final String actionName = "EditCarAction";

  final CarModel? car;
  EditCarAction({
    this.car,
  });
}

class RemoveCarAction {
  final String actionName = "RemoveCarAction";

  final String? carId;
  RemoveCarAction({this.carId});
}

class PostTermsAction {
  final String actionName = "PostTermsAction";
  final TermsModel? terms;

  PostTermsAction({
    this.terms,
  });
}

class SyncTermsAction {
  final String actionName = "SyncTermsAction";
  final TermsModel? termsModel;

  SyncTermsAction({this.termsModel});
}

class SyncAllTermsAction {
  final String actionName = "SyncAllTermsAction";
  final List<TermsModel>? allTerms;

  SyncAllTermsAction({this.allTerms});
}

class GetAllTermsAction {
  final String actionName = "GetTermsAction";

  GetAllTermsAction();
}

class GetTermsAction {
  final String actionName = "GetTermsAction";

  final String? type;

  GetTermsAction({this.type});
}

class EditTermsAction {
  final String actionName = "EditTermsAction";

  final TermsModel? terms;
  EditTermsAction({this.terms});
}

class RemoveTermsAction {
  final String actionName = "RemoveTermsAction";

  final String? termsID;
  RemoveTermsAction({this.termsID});
}
