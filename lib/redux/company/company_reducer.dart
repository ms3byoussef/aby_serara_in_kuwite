import 'package:redux/redux.dart';

import 'company_actions.dart';
import 'company_state.dart';

final companyReducer = combineReducers<CompanyState>([
  TypedReducer<CompanyState, CompanyStatusAction>(_companyAuthState),
  TypedReducer<CompanyState, SyncCarAction>(_syncCar),
  TypedReducer<CompanyState, SyncCarsAction>(_syncCars),
  TypedReducer<CompanyState, RemoveCarAction>(_syncRemoveCar),
  TypedReducer<CompanyState, SyncTermsAction>(_syncTerms),
  TypedReducer<CompanyState, SyncAllTermsAction>(_syncAllTerms),
  TypedReducer<CompanyState, RemoveTermsAction>(_syncRemoveTerms),
]);

CompanyState _companyAuthState(CompanyState state, CompanyStatusAction action) {
  var status = state.status;
  status.update(action.report.actionName!, (v) => action.report,
      ifAbsent: () => action.report);

  return state.copyWith(status: status);
}

CompanyState _syncCar(CompanyState state, SyncCarAction action) {
  state.cars.update(action.car!.id.toString(), (car) => action.car!,
      ifAbsent: () => action.car!);

  return state.copyWith(car: action.car);
}

CompanyState _syncCars(CompanyState state, SyncCarsAction action) {
  for (var car in action.cars ?? []) {
    state.cars.update(car.id.toString(), (cars) => cars, ifAbsent: () => car);
  }

  return state.copyWith(cars: state.cars);
}

CompanyState _syncRemoveCar(CompanyState state, RemoveCarAction action) {
  state.cars.remove(action.carId.toString());
  return state.copyWith(cars: state.cars);
}

CompanyState _syncTerms(CompanyState state, SyncTermsAction action) {
  state.allTerms!.update(
      action.termsModel!.id.toString(), (termsModel) => action.termsModel!,
      ifAbsent: () => action.termsModel!);

  return state.copyWith(terms: action.termsModel);
}

CompanyState _syncAllTerms(CompanyState state, SyncAllTermsAction action) {
  for (var terms in action.allTerms ?? []) {
    state.allTerms!.update(terms.id.toString(), (allTerms) => allTerms,
        ifAbsent: () => terms);
  }

  return state.copyWith(allTerms: state.allTerms);
}

CompanyState _syncRemoveTerms(CompanyState state, RemoveTermsAction action) {
  state.allTerms!.remove(action.termsID.toString());
  return state.copyWith(allTerms: state.allTerms);
}
