// ignore_for_file: avoid_function_literals_in_foreach_calls, avoid_print, no_leading_underscores_for_local_identifiers

import 'package:redux/redux.dart';

import '../../data/repository/client_repository.dart';
import '../action_report.dart';
import '../app/app_state.dart';
import '../company/company_actions.dart';
import 'client_actions.dart';

List<Middleware<AppState>> createClientMiddleware([
  ClientRepository _repository = const ClientRepository(),
]) {
  final postRequest = _postRequest(_repository);
  final getRequestUser = _getRequestUser(_repository);
  final getRequests = _getRequests(_repository);
  final getTerms = _getTerms(_repository);
  final updateRequest = _updateRequest(_repository);
  final removeRequest = _removeRequest(_repository);
  final getCarOwnerCompany = _getCarOwnerCompany(_repository);

  final getClients = _getClients(_repository);

  return [
    TypedMiddleware<AppState, PostRequestAction>(postRequest),
    TypedMiddleware<AppState, GetRequestUserAction>(getRequestUser),
    TypedMiddleware<AppState, GetRequestUserAction>(getCarOwnerCompany),
    TypedMiddleware<AppState, GetRequestsAction>(getRequests),
    TypedMiddleware<AppState, UpdateRequestAction>(updateRequest),
    TypedMiddleware<AppState, RemoveRequestAction>(removeRequest),
    TypedMiddleware<AppState, GetTermsAction>(getTerms),
    TypedMiddleware<AppState, GetClientsAction>(getClients),
  ];
}

Middleware<AppState> _postRequest(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .postRequest(
      action.request,
    )
        .then((request) {
      // next(SyncRequestAction(request: request));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getRequests(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    store.state.clientState!.requests.clear();
    running(next, action);
    repository.getRequests().then((requests) {
      next(SyncRequestsAction(requests: requests));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getRequestUser(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getRequestUser(action.request).then((user) {
      next(SyncRequestUserAction(user: user));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getTerms(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getTerms(action.type).then((terms) {
      next(SyncTermsAction(termsModel: terms));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getCarOwnerCompany(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getCarOwnerUser(action.car).then((user) {
      next(SyncCarOwnerCompanyAction(user: user));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _updateRequest(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.updateRequest(action.requestId, action.status).then((request) {
      next(SyncRequestAction(request: request));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _removeRequest(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .removeRequest(
      action.request,
    )
        .then((request) {
      next(action);
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getClients(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getClients().then((clients) {
      next(SyncClientsAction(clients: clients));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

void catchError(NextDispatcher next, action, error) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: error.toString())));
}

void completed(NextDispatcher next, action) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.complete,
          msg: "${action.actionName} is completed")));
}

void running(NextDispatcher next, action) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.running,
          msg: "${action.actionName} is running")));
}

void idEmpty(NextDispatcher next, action) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: "Id is empty")));
}
