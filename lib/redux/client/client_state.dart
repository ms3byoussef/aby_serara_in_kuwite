// ignore_for_file: prefer_collection_literals

import '../../data/models/car_model.dart';
import '../../data/models/request_model.dart';
import '../../data/models/user_model.dart';
import '../action_report.dart';

class ClientState {
  Map<String, ActionReport> status;
  RequestModel? request;
  CarModel? requestCar;
  final Map<String, RequestModel> requests;
  UserModel? requestUser;
  UserModel? carOwnerUser;
  final UserModel? currentUser;
  final Map<String, UserModel>? clients;

  ClientState({
    required this.status,
    required this.request,
    required this.requests,
    required this.requestUser,
    this.carOwnerUser,
    this.requestCar,
    this.clients,
    required this.currentUser,
  });

  factory ClientState.initial() {
    return ClientState(
        status: Map(),
        request: null,
        requestCar: null,
        requests: Map(),
        carOwnerUser: null,
        requestUser: null,
        currentUser: null,
        clients: Map());
  }

  ClientState copyWith({
    Map<String, ActionReport>? status,
    UserModel? requestUser,
    UserModel? reviewUser,
    UserModel? currentUser,
    UserModel? carOwnerUser,
    RequestModel? request,
    Map<String, RequestModel>? requests,
    CarModel? requestCar,
    Map<String, UserModel>? clients,
  }) {
    return ClientState(
      status: status ?? this.status,
      requestUser: requestUser ?? this.requestUser,
      carOwnerUser: carOwnerUser ?? this.carOwnerUser,
      currentUser: currentUser ?? this.currentUser,
      request: request ?? this.request,
      requestCar: requestCar ?? this.requestCar,
      requests: requests ?? this.requests,
      clients: clients ?? this.clients,
    );
  }
}
