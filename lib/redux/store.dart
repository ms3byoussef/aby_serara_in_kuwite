import 'package:redux/redux.dart';

import 'app/app_reducer.dart';
import 'app/app_state.dart';
import 'auth/auth_middleware.dart';
import 'client/client_middleware.dart';
import 'company/company_middleware.dart';

createStore() {
  return Store<AppState>(
    appReducer,
    initialState: AppState.initial(),
    middleware: [
      ...createAuthMiddleware()
        ..addAll(
          createCompanyMiddleware(),
        )
        ..addAll(createClientMiddleware())
    ],
  );
}
