import '../auth/auth_state.dart';
import '../client/client_state.dart';
import '../company/company_state.dart';

class AppState {
  final AuthState? authState;
  final CompanyState? companyState;
  final ClientState? clientState;

  AppState({this.authState, this.companyState, this.clientState});
  factory AppState.initial() {
    return AppState(
      authState: AuthState.initial(),
      companyState: CompanyState.initial(),
      clientState: ClientState.initial(),
    );
  }
}
