import '../auth/auth_reducer.dart';
import '../client/client_reducer.dart';
import '../company/company_reducer.dart';
import 'app_state.dart';

AppState appReducer(AppState state, dynamic action) {
  return AppState(
    authState: authReducer(state.authState!, action),
    companyState: companyReducer(state.companyState!, action),
    clientState: clientReducer(state.clientState!, action),
  );
}
