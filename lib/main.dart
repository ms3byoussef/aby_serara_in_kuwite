import 'package:device_preview/device_preview.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'feature/presentations/auth/login_screen/login_screen.dart';
import 'feature/presentations/auth/reset_password/reset_password.dart';
import 'feature/presentations/auth/sign_up_screen/sign_up_screen.dart';
import 'feature/presentations/onboarding_screen/splash_screen/splash_screen.dart';
import 'feature/presentations/onboarding_screen/welcome_screen/welcome_screen.dart';
import 'feature/presentations/users_screens/components/edit_profile/edit_profile_screen.dart';
import 'feature/presentations/users_screens/components/profile/profile_screen.dart';
import 'redux/app/app_state.dart';
import 'redux/store.dart';
import 'settings/settings_option.dart';
import 'settings/text_scale.dart';
import 'trans/translations.dart';

main() async {
  var store = await createStore();

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    DevicePreview(
      enabled: false,
      builder: (context) => MyApp(
        store: store,
      ),
    ),
  );
}

class MyApp extends StatefulWidget {
  final Store<AppState> store;

  const MyApp({
    Key? key,
    required this.store,
  }) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  SettingsOptions? _options;
  bool isLogined = false;

  @override
  void initState() {
    super.initState();

    _options = SettingsOptions(
      platform: defaultTargetPlatform,
      textDirection: TextDirection.rtl,
    );
    SharedPreferences.getInstance().then((prefs) {
      var lang = prefs.getString("lang");
      Locale locale = const Locale("en", "US");
      TextDirection textDirection = TextDirection.rtl;
      if (lang == "en") {
        locale = const Locale("en", "US");
        textDirection = TextDirection.ltr;
      } else {
        locale = const Locale("ar", "EG");
        textDirection = TextDirection.rtl;
      }
      setState(() {
        _options = _options!.copyWith(
          languageLocale: locale,
          textDirection: textDirection,
        );
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.store,
      child: MaterialApp(
        title: 'flutter car App',
        debugShowCheckedModeBanner: false,
        locale: _options!.languageLocale,
        routes: _routes(),
        builder: (BuildContext? context, Widget? child) {
          return Directionality(
            textDirection: _options!.textDirection!,
            child: _applyTextScaleFactor(child!),
          );
        },
        localizationsDelegates: const [
          TranslationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale('en', 'US'),
          Locale('ar', 'EG'),
        ],
      ),
    );
  }

  Widget _applyTextScaleFactor(Widget child) {
    return Builder(
      builder: (BuildContext context) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(
            textScaleFactor: Translations.of(context)!.currentLanguage == 'en'
                ? appTextScaleValues[1].scale
                : appTextScaleValues[2].scale,
          ),
          child: child,
        );
      },
    );
  }

  void _handleOptionsChanged(SettingsOptions newOptions) {
    if (newOptions.languageLocale!.languageCode !=
        _options!.languageLocale!.languageCode) {
      SharedPreferences.getInstance().then((pref) {
        pref.setString("lang", newOptions.languageLocale!.languageCode);
      });
    }
    setState(() {
      _options = newOptions;
    });
  }

  Map<String, WidgetBuilder> _routes() {
    return <String, WidgetBuilder>{
      "/welcome": (_) => const WelcomeScreen(),
      "/login": (_) => LoginScreen(
            options: _options!,
            onOptionsChanged: _handleOptionsChanged,
          ),
      "/sign_up": (_) => SignUpScreen(
            options: _options!,
            onOptionsChanged: _handleOptionsChanged,
          ),
      "/edit_profile": (_) => EditProfileScreen(
            options: _options!,
            onOptionsChanged: _handleOptionsChanged,
          ),
      "/profile": (_) => const ProfileScreen(),
      "/": (_) => const SplashScreen(),
      "/reset_password": (_) => const ResetPassword(),
    };
  }
}
