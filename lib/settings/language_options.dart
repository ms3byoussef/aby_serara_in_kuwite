// ignore_for_file: unused_field

import 'dart:ui';

// ignore: constant_identifier_names
enum AppLanguageName { AR, EN }

class AppLanguage {
  static final AppLanguage _singleton = AppLanguage._internal();
  static AppLanguageName _languageName = AppLanguageName.EN;
  static const Map<AppLanguageName, Locale> supportLanguages = {
    AppLanguageName.AR: Locale('ar', 'EG'),
    AppLanguageName.EN: Locale('en', 'US'),
  };

  factory AppLanguage() {
    return _singleton;
  }

  AppLanguage._internal();

  static void configure(AppLanguageName languageName) {
    _languageName = languageName;
  }
}

class AppLanguageValue {
  String? language;
  Locale? locale;
  TextDirection textDirection;
  AppLanguageValue(
      {this.language, this.locale, this.textDirection = TextDirection.ltr});
}

List<AppLanguageValue> appLanguageValues = <AppLanguageValue>[
  AppLanguageValue(
      language: "عربي",
      locale: const Locale('ar', 'EG'),
      textDirection: TextDirection.rtl),
  AppLanguageValue(
    language: "English",
    locale: const Locale('en', 'US'),
  ),
];
